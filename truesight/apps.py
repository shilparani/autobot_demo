from django.apps import AppConfig


class TruesightConfig(AppConfig):
    name = 'truesight'
