from django.shortcuts import render
from django.http import JsonResponse
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login
from django.contrib import messages
import MySQLdb, collections, json, datetime, itertools, datetime, pymssql
from . import utils
import sys
#import ldap
#sys.path.insert(0,'/u01/app/autobot/autobot')
#from ..autobot import settings
sys.path.insert(0,'/var/www/html/autobot/autobot')
import settings


# Create your views here.
def get_autobot_connection():
    conn = MySQLdb.connect(user='root', passwd='Neurones@2018', host='10.236.0.3', db='demo_autobot', charset='utf8')
    #conn = MySQLdb.connect(user='Autobot_Restitution', password='sebrocks', host='NLVCP2008P.d1.cougar.ms.lvmh', database='Autobot')
    return conn

def get_autobot_dev_connection():
    conn = MySQLdb.connect(user='root', passwd='Neurones@2018', host='10.236.0.3', db='demo_autobot', charset='utf8')
    #conn = MySQLdb.connect(user='Autobot_Restitution', password='98UAxDhm', host='nlvcp2064d.d1.cougar.ms.lvmh', database='Autobot')
    return conn

def try_json(request):

    print("version:%s"%sys.version)
    conn = utils.get_autobot_connection()
    try:
        cursor = conn.cursor()
        
        alerts_month = ["SELECT month(`DATE_ts`),count(*) FROM events where type = 'In Maintenance' and (`DATE_ts` >= '2019-01-20' - INTERVAL 2 month) Group by month(`DATE_ts`) order by date_ts;"
        ,"SELECT month(`DATE_ts`),count(Ticket) FROM events where (`DATE_ts` >= '2019-01-20' - INTERVAL 2 month) AND type = 'Under Reboot' Group by month(`DATE_ts`) order by date_ts; "
        ,"SELECT month(`DATE_ts`),count(evt.Ticket) FROM events evt where (`DATE_ts` >= '2019-01-20' - INTERVAL 2 month) and evt.type = 'Multiple' Group by month(`DATE_ts`) order by date_ts;"
        ,"SELECT month(`DATE_ts`),count(Distinct(evt.ts_alert)) FROM events evt where (`DATE_ts` >= '2019-01-20' - INTERVAL 2 month) and evt.id not in  (select id from events where type = 'Multiple' OR type = 'MASSIVE' or type = 'In maintenance' or type = 'Under reboot') Group by month(`DATE_ts`) order by date_ts;"
        ,"SELECT month(`DATE_ts`),count(Distinct(evt.ts_alert)) FROM events evt where (`DATE_ts` >= '2019-01-20' - INTERVAL 2 month) and type = 'MASSIVE' Group by month(`DATE_ts`) order by date_ts;"]        

        alerts_query = [alerts_month]
        t_res = []
        alert_cnt2 = dict()
        alert_pct2 = dict()
        key2 = ['month']
        total_alerts_cnt2 = 0 

        for j in range(0,1):
            queries = alerts_query[j]
            tab = []
            for i in range(0,5):
                querie = queries[i]
                cursor.execute(querie)
                res_row = cursor.fetchall()
                t = []
                if j == 0: #day
                    t = utils.correctArray(res_row,1)
                
                rows = cursor.fetchall()
                tab.append(t)
                
                if i== 0:
                    in_maintenance_cnt2 = t[-1]
                if i== 1:    
                    under_reboot_cnt2 = t[-1]
                if i== 2:
                    duplicate_cnt2 = t[-1]
                if i== 3:
                    non_duplicate_cnt2 = t[-1]
                if i== 4:
                    massive_cnt2 = t[-1]

            total_alerts_cnt2 = in_maintenance_cnt2 + duplicate_cnt2 + non_duplicate_cnt2 + under_reboot_cnt2 + massive_cnt2
            alert_cnt2[key2[j]] = [in_maintenance_cnt2,under_reboot_cnt2,duplicate_cnt2,non_duplicate_cnt2,massive_cnt2]
            alert_pct2[key2[j]] = {'in_maintenance_pct':"{0:.0f}%".format(in_maintenance_cnt2/total_alerts_cnt2 * 100),'under_reboot_cnt':"{0:.0f}%".format(under_reboot_cnt2/total_alerts_cnt2 * 100),'duplicate_pct':"{0:.0f}%".format(duplicate_cnt2/total_alerts_cnt2 * 100),'non_duplicate_pct': "{0:.0f}%".format(non_duplicate_cnt2/total_alerts_cnt2 * 100),'massive_pct': "{0:.0f}%".format(massive_cnt2/total_alerts_cnt2 * 100)} if total_alerts_cnt2 !=0 else {'in_maintenance_pct2':'0','under_reboot_cnt2':'0','duplicate_pct2':'0','non_duplicate_pct2':'0','massive_pct2':'0'}

            tot = [sum(x) for x in zip(*tab)]
            tab.append(tot)
            t_res.append(tab)

    finally:
        conn.close()

    context ={
        'today': datetime.datetime.today().strftime('%Y-%m-%d'),
        'alerts_cnt':alert_cnt2,
        'alerts_pct':alert_pct2,
        #'tickets_cnt':ticket_cnt2,
        #'tickets_pct':ticket_pct2,
        # 'alert_trend_data':alert_trend_data,
        #'tickets_trend_data':ticket_trend_data,
        #'tickets_trend_data2':ticket_trend_data2,
        'arr_alerts':t_res,
        #'arr_tickets':tickets_res,
    }
    return render(request, 'try_json.html', context)

def try_jsonTicket(request):
    print("version:%s"%sys.version)
    conn = utils.get_autobot_connection()
    try:
        cursor = conn.cursor()
        #tickets trend
        ticket_trend_query = []
        ticket_trend_query_month = [
        "select Month(`DATE_ts`), count(DISTINCT(ticket)) from events where (type = 'Creation' OR type= 'Massive' OR type = 'Multiple') and ( `DATE_ts` >= '2019-01-20' - INTERVAL 3 MONTH) Group by Month(`DATE_ts`) order by date_ts;"
        ,"select Month(`DATE_ts`), count(*) from events where type = 'Closed' and ( `DATE_ts` >= '2019-01-20' - INTERVAL 3 MONTH) Group by Month(`DATE_ts`) order by date_ts;"
        ,"select Month(`DATE_ts`), count(*) from events where type = 'First analyse' and ( `DATE_ts` >= '2019-01-20' - INTERVAL 3 MONTH) Group by Month(`DATE_ts`) order by date_ts; "
        ,"select Month(`DATE_ts`), count(*) from events where type = 'Increment' and ( `DATE_ts` >= '2019-01-20' - INTERVAL 3 MONTH) Group by Month(`DATE_ts`) order by date_ts; "
        ,"select Month(`DATE_ts`), count(*) from history hst, events ev where hst.action = 'Reopening Ticket' and ev.id = hst.id and ( `DATE_ts` >='2019-01-20' - INTERVAL 3 MONTH) Group by Month(`DATE_ts`) order by date_ts;"
        ,"select Month(`DATE_ts`),count(*) from events where type = 'Forward' and ( `DATE_ts` >= '2019-01-20' - INTERVAL 3 MONTH) Group by Month(`DATE_ts`) order by date_ts;"
        ]
    # ticket_trend_query = ticket_trend_query_day

        tickets_query = [ticket_trend_query_month]
        
        tickets_res = []
        ticket_cnt2 = dict()
        ticket_pct2 = dict()
        key2 = ['month']
        total_ticket_cnt2 = 0 
        
        for j in range(0,1):
            queries = tickets_query[j]
            tab_tickets = []
            for i in range(0,6):
                querie = queries[i]
                cursor.execute(querie)
                res_row = cursor.fetchall()
                h = []
                if j == 0: #day
                    h = utils.correctArray(res_row,1)
                

                tab_tickets.append(h)
                
                if i== 0:
                    created_ticket_cnt2 = h[-1]
                if i== 1:    
                    closed_ticket_cnt2 = h[-1]
                if i== 2:
                    first_analysis_cnt2 = h[-1]
                if i== 3:
                    increment_cnt2 = h[-1]
                if i== 4:
                    reopen_cnt2 = h[-1]
                if i== 5:
                    forward_cnt2 = h[-1]

            total_ticket_cnt2 = created_ticket_cnt2 + forward_cnt2 + closed_ticket_cnt2 + first_analysis_cnt2 + increment_cnt2 + reopen_cnt2
   
            ticket_cnt2[key2[j]] = [created_ticket_cnt2, forward_cnt2, increment_cnt2, first_analysis_cnt2, closed_ticket_cnt2, reopen_cnt2]
            ticket_pct2[key2[j]] = {"created":"{0:.0f}%".format(float(created_ticket_cnt2)/float(total_ticket_cnt2) * 100),"forward":"{0:.0f}%".format(float(forward_cnt2)/float(total_ticket_cnt2) * 100),"increment":"{0:.0f}%".format(float(increment_cnt2)/float(total_ticket_cnt2) * 100),"first":"{0:.0f}%".format(float(first_analysis_cnt2)/float(total_ticket_cnt2) * 100),"closed":"{0:.0f}%".format(float(closed_ticket_cnt2)/float(total_ticket_cnt2) * 100),"reopen":"{0:.0f}%".format(float(reopen_cnt2)/float(total_ticket_cnt2) * 100)} if total_ticket_cnt2!=0 else {"created":'0',"forward":'0',"increment":'0',"first":'0',"closed":'0',"reopen":'0'}
                 
            toto = [sum(x) for x in zip(*tab_tickets)]
            tab_tickets.append(toto)
            tickets_res.append(tab_tickets)
        
    finally:
        conn.close()

    context ={
        'tickets_cnt':ticket_cnt2,
        'tickets_pct':ticket_pct2,
        # 'alert_trend_data':alert_trend_data,
        #'tickets_trend_data':ticket_trend_data,
        #'tickets_trend_data2':ticket_trend_data2,
        'arr_tickets':tickets_res,
    }
    return render(request, 'try_jsonTicket.html', context)

def try_jsonAlertYear(request):
    print("version:%s"%sys.version)
    conn = utils.get_autobot_connection()
    try:
        cursor = conn.cursor()

        alerts_year = ["SELECT YEAR('2019-01-20'),count(*) FROM events where type = 'In Maintenance' and (date_ts) >= DATE('2019-01-20'-INTERVAL 1 YEAR)"
        ,"SELECT YEAR('2019-01-20'),count(Ticket) FROM events where (date_ts) >= DATE('2019-01-20'-INTERVAL 1 YEAR) AND type = 'Under Reboot'"
        ,"SELECT YEAR('2019-01-20'),count(evt.Ticket) FROM events evt where (date_ts) >= DATE('2019-01-20'-INTERVAL 1 YEAR) and evt.type = 'Multiple' Group by year('2019-01-20')"
        ,"SELECT YEAR('2019-01-20'),count(Distinct(evt.ts_alert)) FROM events evt where (date_ts) >= DATE('2019-01-20'-INTERVAL 1 YEAR) and evt.id not in  (select id from events where type = 'Multiple' OR type = 'MASSIVE' or type = 'In maintenance' or type = 'Under reboot')"
        ,"SELECT YEAR('2019-01-20'),count(Distinct(evt.ts_alert)) FROM events evt where (date_ts) >= DATE('2019-01-20'-INTERVAL 1 YEAR) and type = 'MASSIVE'"]        

        alerts_query = [alerts_year]
        t_res = []
        alert_cnt2 = dict()
        alert_pct2 = dict()
        key2 = ['year']
        total_alerts_cnt2 = 0 
        
        for j in range(0,1):
            queries = alerts_query[j]
            tab = []
            for i in range(0,5):
                querie = queries[i]
                cursor.execute(querie)
                res_row = cursor.fetchall()
                t = []
                if j == 0: #year
                    t = utils.correctArray(res_row,3)
                
                
                rows = cursor.fetchall()
                tab.append(t)
                
                if i== 0:
                    in_maintenance_cnt2 = t[-1]
                if i== 1:    
                    under_reboot_cnt2 = t[-1]
                if i== 2:
                    duplicate_cnt2 = t[-1]
                if i== 3:
                    non_duplicate_cnt2 = t[-1]
                if i== 4:
                    massive_cnt2 = t[-1]
                    
            total_alerts_cnt2 = in_maintenance_cnt2 + duplicate_cnt2 + non_duplicate_cnt2 + under_reboot_cnt2 + massive_cnt2
            alert_cnt2[key2[j]] = [in_maintenance_cnt2,under_reboot_cnt2,duplicate_cnt2,non_duplicate_cnt2,massive_cnt2]
            alert_pct2[key2[j]] = {'in_maintenance_pct':"{0:.0f}%".format(in_maintenance_cnt2/total_alerts_cnt2 * 100),'under_reboot_cnt':"{0:.0f}%".format(under_reboot_cnt2/total_alerts_cnt2 * 100),'duplicate_pct':"{0:.0f}%".format(duplicate_cnt2/total_alerts_cnt2 * 100),'non_duplicate_pct': "{0:.0f}%".format(non_duplicate_cnt2/total_alerts_cnt2 * 100),'massive_pct': "{0:.0f}%".format(massive_cnt2/total_alerts_cnt2 * 100)} if total_alerts_cnt2 !=0 else {'in_maintenance_pct2':'0','under_reboot_cnt2':'0','duplicate_pct2':'0','non_duplicate_pct2':'0','massive_pct2':'0'}
            
            tot = [sum(x) for x in zip(*tab)]
            tab.append(tot)
            t_res.append(tab)
         
        
    finally:
        conn.close()

    context ={
        'today': datetime.datetime.today().strftime('%Y-%m-%d'),
        'alerts_cnt':alert_cnt2,
        'alerts_pct':alert_pct2,
        #'tickets_cnt':ticket_cnt2,
        #'tickets_pct':ticket_pct2,
        # 'alert_trend_data':alert_trend_data,
        #'tickets_trend_data':ticket_trend_data,
        #'tickets_trend_data2':ticket_trend_data2,
        'arr_alerts':t_res,
        #'arr_tickets':tickets_res,
    }
    return render(request, 'try_jsonAlertYear.html', context)

def try_jsonTicketYear(request):
    print("version:%s"%sys.version)
    conn = utils.get_autobot_connection()
    try:
        cursor = conn.cursor()

        ticket_trend_query = []
        
        ticket_trend_query_year = ["select YEAR('2019-01-20'), count(DISTINCT(ticket)) from events where (type = 'Creation' OR type= 'Massive' OR type = 'Multiple') and (date_ts) >= DATE('2019-01-20'-INTERVAL 1 YEAR);"
        ,"select YEAR('2019-01-20'), count(*) from events where type = 'Closed' and (date_ts) >= DATE('2019-01-20'-INTERVAL 1 YEAR);"
        ,"select YEAR('2019-01-20'), count(*) from events where type = 'First analyse' and (date_ts) >= DATE('2019-01-20'-INTERVAL 1 YEAR);"
        ,"select YEAR('2019-01-20'), count(*) from events where type = 'Increment' and (date_ts) >= DATE('2019-01-20'-INTERVAL 1 YEAR);"
        ,"select YEAR('2019-01-20'), count(*) from history where action = 'Reopening Ticket' and (date) >= DATE('2019-01-20'-INTERVAL 1 YEAR);"
        ,"select YEAR('2019-01-20'),count(*) from events where type = 'Forward' and (date_ts) >= DATE('2019-01-20'-INTERVAL 1 YEAR);"]
        tickets_query = [ticket_trend_query_year]
        
        tickets_res = []
        ticket_cnt2 = dict()
        ticket_pct2 = dict()
        key2 = ['year']
        total_ticket_cnt2 = 0 
        
        for j in range(0,1):
            queries = tickets_query[j]
            tab_tickets = []
            for i in range(0,6):
                querie = queries[i]
                cursor.execute(querie)
                res_row = cursor.fetchall()
                h = []
                if j == 0: #day
                    h = utils.correctArray(res_row,3)
                

                tab_tickets.append(h)
                
                if i== 0:
                    created_ticket_cnt2 = h[-1]
                if i== 1:    
                    closed_ticket_cnt2 = h[-1]
                if i== 2:
                    first_analysis_cnt2 = h[-1]
                if i== 3:
                    increment_cnt2 = h[-1]
                if i== 4:
                    reopen_cnt2 = h[-1]
                if i== 5:
                    forward_cnt2 = h[-1]

            total_ticket_cnt2 = created_ticket_cnt2 + forward_cnt2 + closed_ticket_cnt2 + first_analysis_cnt2 + increment_cnt2 + reopen_cnt2
   
            ticket_cnt2[key2[j]] = [created_ticket_cnt2, forward_cnt2, increment_cnt2, first_analysis_cnt2, closed_ticket_cnt2, reopen_cnt2]
            ticket_pct2[key2[j]] = {"created":"{0:.0f}%".format(float(created_ticket_cnt2)/float(total_ticket_cnt2) * 100),"forward":"{0:.0f}%".format(float(forward_cnt2)/float(total_ticket_cnt2) * 100),"increment":"{0:.0f}%".format(float(increment_cnt2)/float(total_ticket_cnt2) * 100),"first":"{0:.0f}%".format(float(first_analysis_cnt2)/float(total_ticket_cnt2) * 100),"closed":"{0:.0f}%".format(float(closed_ticket_cnt2)/float(total_ticket_cnt2) * 100),"reopen":"{0:.0f}%".format(float(reopen_cnt2)/float(total_ticket_cnt2) * 100)} if total_ticket_cnt2!=0 else {"created":'0',"forward":'0',"increment":'0',"first":'0',"closed":'0',"reopen":'0'}
                 
            toto = [sum(x) for x in zip(*tab_tickets)]
            tab_tickets.append(toto)
            tickets_res.append(tab_tickets)
        
    finally:
        conn.close()

    context ={
        'tickets_cnt':ticket_cnt2,
        'tickets_pct':ticket_pct2,
        # 'alert_trend_data':alert_trend_data,
        #'tickets_trend_data':ticket_trend_data,
        #'tickets_trend_data2':ticket_trend_data2,
        'arr_tickets':tickets_res,
    }
    return render(request, 'try_jsonTicketYear.html', context)


def truesight_dashboard(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        print("version:%s"%sys.version)
        conn = utils.get_autobot_connection()
        try:
            cursor = conn.cursor()

            alerts_day = ["SELECT Dayofyear(date_ts),count(*) FROM events where type = 'In Maintenance' and DATE(date_ts) >= '2019-01-20' - INTERVAL 6 DAY group by Dayofyear(date_ts) order by Dayofyear(date_ts) LIMIT 7"
            ,"SELECT Dayofyear(date_ts),count(Ticket) FROM events where Date(date_ts) >= '2019-01-20' - INTERVAL 6 DAY AND type = 'Under Reboot' group by Dayofyear(date_ts) order by Dayofyear(date_ts) LIMIT 7"
            ,"SELECT Dayofyear(evt.date_ts),count(evt.Ticket) FROM events evt where Date(evt.date_ts) >= '2019-01-20' - INTERVAL 6 DAY and evt.type = 'Multiple' GROUP BY Dayofyear(evt.date_ts) order by Dayofyear(evt.date_ts) LIMIT 7 "
            ,"SELECT DayOfYear(evt.date_ts),count(Distinct(evt.ts_alert)) FROM events evt where Date(evt.date_ts) >= '2019-01-20' - INTERVAL 6 Day and evt.id not in  (select id from events where type = 'Multiple' OR type = 'MASSIVE' or type = 'In maintenance' or type = 'Under reboot') group by DayOfYear(evt.date_ts) order by DayOfYear(evt.date_ts) desc LIMIT 7"
            ,"SELECT DayOfYear(evt.date_ts),count(Distinct(evt.ts_alert)) FROM events evt where Date(evt.date_ts) >= '2019-01-20' - INTERVAL 6 Day and type = 'MASSIVE' group by DayOfYear(evt.date_ts) order by DayOfYear(evt.date_ts) LIMIT 7"]

            alerts_week = ["SELECT WEEK(date_ts,1),count(*) FROM events where type = 'In Maintenance' and DATE(date_ts) >= '2019-01-20' - INTERVAL 2 WEEK group by WEEK(date_ts,1) ; "
            ,"SELECT Week(date_ts,1),count(Ticket) FROM events where Date(date_ts) >='2019-01-20' - INTERVAL 2 Week AND type = 'Under Reboot' group by Week(date_ts,1) order by Week(date_ts,1) ; "
            ,"SELECT Week(evt.date_ts,1),count(evt.Ticket) FROM events evt where Date(evt.date_ts) >= '2019-01-20' - INTERVAL 2 Week and evt.type = 'Multiple' GROUP BY Week(evt.date_ts,1); "
            ,"SELECT WEEK(date_ts,1),count(Distinct(evt.ts_alert)) FROM events evt where Date(evt.date_ts) >= '2019-01-20' - INTERVAL 2 Week and evt.id not in  (select id from events where type = 'Multiple' OR type = 'MASSIVE' or type = 'In maintenance' or type = 'Under reboot') GROUP BY Week(DATE_SUB(evt.date_ts, INTERVAL 1 DAY)) order by Week(DATE_SUB(evt.date_ts, INTERVAL 1 DAY)) ;"
            ,"SELECT WEEK(date_ts,1),count(Distinct(evt.ts_alert)) FROM events evt where Date(evt.date_ts) >= '2019-01-20' - INTERVAL 2 Week and type = 'MASSIVE' GROUP BY Week(DATE_SUB(evt.date_ts, INTERVAL 1 DAY)) order by Week(DATE_SUB(evt.date_ts, INTERVAL 1 DAY))"]        

            alerts_query = [alerts_day,alerts_week]
            t_res = []
            alert_cnt2 = dict()
            alert_pct2 = dict()
            key2 = ['day','week','month','year']
            total_alerts_cnt2 = 0 

            for j in range(0,2):
                queries = alerts_query[j]
                tab = []
                for i in range(0,5):
                    querie = queries[i]
                    cursor.execute(querie)
                    res_row = cursor.fetchall()
                    t = []
                    if j == 0: #day
                        t = utils.correctArray(res_row,2)
                    elif j == 1:#week
                        t = utils.correctArray(res_row,0)

                    rows = cursor.fetchall()
                    t = [int(a) for a in t]
                    tab.append(t)

                    if i== 0:
                        in_maintenance_cnt2 = t[-1]
                    if i== 1:    
                        under_reboot_cnt2 = t[-1]
                    if i== 2:
                        duplicate_cnt2 = t[-1]
                    if i== 3:
                        non_duplicate_cnt2 = t[-1]
                    if i== 4:
                        massive_cnt2 = t[-1]

                total_alerts_cnt2 = in_maintenance_cnt2 + duplicate_cnt2 + non_duplicate_cnt2 + under_reboot_cnt2 + massive_cnt2
                alert_cnt2[key2[j]] = [in_maintenance_cnt2,under_reboot_cnt2,duplicate_cnt2,non_duplicate_cnt2,massive_cnt2]
                alert_pct2[key2[j]] = {'in_maintenance_pct':"{0:.0f}%".format(in_maintenance_cnt2/total_alerts_cnt2 * 100),'under_reboot_cnt':"{0:.0f}%".format(under_reboot_cnt2/total_alerts_cnt2 * 100),'duplicate_pct':"{0:.0f}%".format(duplicate_cnt2/total_alerts_cnt2 * 100),'non_duplicate_pct': "{0:.0f}%".format(non_duplicate_cnt2/total_alerts_cnt2 * 100),'massive_pct': "{0:.0f}%".format(massive_cnt2/total_alerts_cnt2 * 100)} if total_alerts_cnt2 !=0 else {'in_maintenance_pct2':'0','under_reboot_cnt2':'0','duplicate_pct2':'0','non_duplicate_pct2':'0','massive_pct2':'0'}

                tot = [sum(x) for x in zip(*tab)]
                tab.append(tot) 
                t_res.append(tab)

            #tickets trend
            ticket_trend_query = []

            ticket_trend_query_day = [
            "select dayofyear(`DATE_ts`), count(*) from events where (type = 'Creation' OR type= 'Massive' OR type = 'Multiple') and (`DATE_ts` >= '2019-01-20' - INTERVAL 6 DAY) Group by dayofyear(`DATE_ts`) order by dayofyear(`DATE_ts`);"
            ,"select dayofyear(`DATE_ts`), count(*) from events where type = 'Closed' and (`DATE_ts` >= '2019-01-20' - INTERVAL 6 DAY) Group by dayofyear(`DATE_ts`) order by dayofyear(`DATE_ts`);"
            ,"select dayofyear(`DATE_ts`), count(*) from events where type = 'First analyse' and (`DATE_ts` >= '2019-01-20' - INTERVAL 6 DAY) Group by dayofyear(`DATE_ts`) order by dayofyear(`DATE_ts`); "
            ,"select DAYofYear(`DATE_ts`), count(*) from events where type = 'Increment' and (`DATE_ts` >= '2019-01-20' - INTERVAL 6 DAY) Group by dayofyear(`DATE_ts`) order by dayofyear(`DATE_ts`); "
            ,"select DayOfYear(`DATE`), count(*) from history where action = 'Reopening Ticket' and (`DATE` >= '2019-01-20'- INTERVAL 6 DAY) Group by dayofyear(`DATE`) order by dayofyear(`DATE`);"
            ,"select DayOfYear(date_ts),count(*) from events where type = 'Forward' and (date_ts >= '2019-01-20'- INTERVAL 6 DAY) Group by DAYofYear(date_ts) order by DAYofYear(date_ts);"
            ]

            ticket_trend_query_week = ["select Week(date_ts, 1), count(*) from events where (type = 'Creation' OR type= 'Massive' OR type = 'Multiple') and (`DATE_ts` >= '2019-01-20' - INTERVAL 2 WEEK) group by Week(date_ts, 1) order by date_ts;"
            ,"select Week(date_ts, 1), count(*) from events where type = 'Closed' and (`DATE_ts` >= '2019-01-20' - INTERVAL 2 WEEK) group by Week(date_ts, 1) order by date_ts;"
            ,"select Week(date_ts, 1), count(*) from events where type = 'First analyse' and (`DATE_ts` >= '2019-01-20' - INTERVAL 2 WEEK) group by Week(date_ts, 1) order by date_ts;"
            ,"select Week(date_ts, 1), count(*) from events where type = 'Increment' and (`DATE_ts` >= '2019-01-20' - INTERVAL 2 WEEK) group by Week(date_ts, 1) order by date_ts;"
            ,"select Week(date, 1), count(*) from history where action = 'Reopening Ticket' and (`DATE` >= '2019-01-20'- INTERVAL 2 Week) group by Week(date, 1) order by date;"
            ,"select Week(date_ts, 1), count(*) from events where type = 'Forward' and (DATE_ts >= '2019-01-20'- INTERVAL 2 Week) group by Week(date_ts, 1) order by date_ts;"]



            # ticket_trend_query = ticket_trend_query_day

            tickets_query = [ticket_trend_query_day,ticket_trend_query_week]

            tickets_res = []
            ticket_cnt2 = dict()
            ticket_pct2 = dict()
            key2 = ['day','week']
            total_ticket_cnt2 = 0 

            for j in range(0,2):
                queries = tickets_query[j]
                tab_tickets = []
                for i in range(0,6):
                    querie = queries[i]
                    cursor.execute(querie)
                    res_row = cursor.fetchall()
                    h = []
                    if j == 0: #day
                        h = utils.correctArray(res_row,2)
                    elif j == 1:#week
                        h = utils.correctArray(res_row,0)
                    elif j == 2:#month
                        h = utils.correctArray(res_row,1)
                    elif j == 3:#year
                       h = utils.correctArray(res_row,3)

                    h = [int(a) for a in h]
                    tab_tickets.append(h)

                    if i== 0:
                        created_ticket_cnt2 = h[-1]
                    if i== 1:    
                        closed_ticket_cnt2 = h[-1]
                    if i== 2:
                        first_analysis_cnt2 = h[-1]
                    if i== 3:
                        increment_cnt2 = h[-1]
                    if i== 4:
                        reopen_cnt2 = h[-1]
                    if i== 5:
                        forward_cnt2 = h[-1]

                total_ticket_cnt2 = created_ticket_cnt2 + forward_cnt2 + closed_ticket_cnt2 + first_analysis_cnt2 + increment_cnt2 + reopen_cnt2

                ticket_cnt2[key2[j]] = [created_ticket_cnt2, forward_cnt2, increment_cnt2, first_analysis_cnt2, closed_ticket_cnt2, reopen_cnt2]
                ticket_pct2[key2[j]] = {"created":"{0:.0f}%".format(float(created_ticket_cnt2)/float(total_ticket_cnt2) * 100),"forward":"{0:.0f}%".format(float(forward_cnt2)/float(total_ticket_cnt2) * 100),"increment":"{0:.0f}%".format(float(increment_cnt2)/float(total_ticket_cnt2) * 100),"first":"{0:.0f}%".format(float(first_analysis_cnt2)/float(total_ticket_cnt2) * 100),"closed":"{0:.0f}%".format(float(closed_ticket_cnt2)/float(total_ticket_cnt2) * 100),"reopen":"{0:.0f}%".format(float(reopen_cnt2)/float(total_ticket_cnt2) * 100)} if total_ticket_cnt2!=0 else {"created":'0',"forward":'0',"increment":'0',"first":'0',"closed":'0',"reopen":'0'}

                toto = [sum(x) for x in zip(*tab_tickets)]
                tab_tickets.append(toto)
                tickets_res.append(tab_tickets)

        finally:
            conn.close()

        #hardcoding context for demo
        '''alert_cnt2 = {'week': [52, 192, 25, 360, 50], 'day': [2, 15, 0, 19, 0]}
        alert_pct2 = {'week': {'massive_pct': '0%', 'non_duplicate_pct': '0%', 'duplicate_pct': '0%', 'under_reboot_cnt': '0%', 'in_maintenance_pct': '0%'}, 'day': {'massive_pct': '0%', 'non_duplicate_pct': '0%', 'duplicate_pct': '0%', 'under_reboot_cnt': '0%', 'in_maintenance_pct': '0%'}}
        ticket_cnt2 = {'week': [83, 153, 120, 78, 1, 0], 'day': [0, 6, 9, 4, 0, 0]}
        ticket_pct2 = {'week': {'increment': '28%', 'created': '19%', 'reopen': '0%', 'closed': '0%', 'forward': '35%', 'first': '18%'}, 'day': {'increment': '47%', 'created': '0%', 'reopen': '0%', 'closed': '0%', 'forward': '32%', 'first': '21%'}}
        t_res = [[[4, 1, 5, 40, 7, 3, 2], [0, 0, 36, 54, 80, 43, 15], [0, 0, 7, 20, 5, 0, 0], [73, 40, 57, 174, 85, 82, 19], [0, 0, 0, 50, 0, 0, 0], [77, 41, 105, 338, 177, 128, 36]], [[108, 76, 52], [3, 82, 192], [244, 45, 25], [479, 765, 360], [327, 111, 50], [1161, 1079, 679]]]
        tickets_res = [[[12, 0, 12, 75, 6, 2, 0], [0, 0, 0, 0, 0, 1, 0], [7, 7, 6, 42, 9, 23, 4], [36, 26, 24, 68, 23, 20, 9], [0, 0, 0, 0, 0, 0, 0], [18, 7, 22, 59, 52, 36, 6], [73, 40, 64, 244, 90, 82, 19]], [[591, 183, 83], [5, 2, 1], [110, 231, 78], [156, 255, 120], [0, 0, 0], [188, 250, 153], [1050, 921, 435]]]'''
        
        context ={
            'today': datetime.datetime.today().strftime('%Y-%m-%d'),
            'alerts_cnt':alert_cnt2,
            'alerts_pct':alert_pct2,
            'tickets_cnt':ticket_cnt2,
            'tickets_pct':ticket_pct2,
            # 'alert_trend_data':alert_trend_data,
            #'tickets_trend_data':ticket_trend_data,
            #'tickets_trend_data2':ticket_trend_data2,
            'arr_alerts':t_res,
            'arr_tickets':tickets_res,
        }
        #import pdb;pdb.set_trace()
        return render(request, 'truesight_dashboard.html', context)
        
def workflow(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        conn = utils.get_autobot_connection()
        try:
            cursor = conn.cursor()

            workflow_day = [
            "SELECT Dayofyear(date_ts),count(*) FROM events where workflow like '%Get-Size' and DATE(date_ts) >= '2019-01-20' - INTERVAL 6 DAY group by Dayofyear(date_ts) order by date_ts LIMIT 7"
            ,"SELECT Dayofyear(date_ts),count(*) FROM events where workflow like '%restart-service' and DATE(date_ts) >= '2019-01-20' - INTERVAL 6 DAY group by Dayofyear(date_ts) order by date_ts LIMIT 7 "
            ,"SELECT Dayofyear(date_ts),count(*) FROM events where workflow like '%Restart-Server' and DATE(date_ts) >= '2019-01-20' - INTERVAL 6 DAY group by Dayofyear(date_ts) order by date_ts LIMIT 7 "
            ,"SELECT Dayofyear(date_ts),count(*) FROM events where workflow like '%Restart-WinRM' and DATE(date_ts) >= '2019-01-20' - INTERVAL 6 DAY group by Dayofyear(date_ts) order by date_ts LIMIT 7 "
            ,"SELECT Dayofyear(date_ts),count(*) FROM events where workflow like '%Get-RAM' and DATE(date_ts) >= '2019-01-20' - INTERVAL 6 DAY group by Dayofyear(date_ts) order by date_ts LIMIT 7 "
            ,"SELECT Dayofyear(date_ts),count(*) FROM events where workflow like '%Get-cpu' and DATE(date_ts) >= '2019-01-20' - INTERVAL 6 DAY group by Dayofyear(date_ts) order by date_ts LIMIT 7 "
            ]

            workflow_week = [
            "SELECT WEEK(date_ts,1),count(*) FROM events where workflow like '%Get-Size' and DATE(date_ts) >= '2019-01-20' - INTERVAL 2 WEEK group by WEEK(date_ts,1) order by date_ts LIMIT 3"
            ,"SELECT WEEK(date_ts,1),count(*) FROM events where workflow like '%restart-service' and DATE(date_ts) >= '2019-01-20' - INTERVAL 2 WEEK group by WEEK(date_ts,1) order by date_ts LIMIT 3 "
            ,"SELECT WEEK(date_ts,1),count(*) FROM events where workflow like '%Restart-Server' and DATE(date_ts) >= '2019-01-20' - INTERVAL 2 WEEK group by WEEK(date_ts,1) order by date_ts LIMIT 3"
            ,"SELECT WEEK(date_ts,1),count(*) FROM events where workflow like '%Restart-WinRM' and DATE(date_ts) >= '2019-01-20' - INTERVAL 2 WEEK group by WEEK(date_ts,1) order by date_ts LIMIT 3 "
            ,"SELECT WEEK(date_ts,1),count(*) FROM events where workflow like '%Get-RAM' and DATE(date_ts) >= '2019-01-20' - INTERVAL 2 WEEK group by WEEK(date_ts,1) order by date_ts LIMIT 3"
            ,"SELECT WEEK(date_ts,1),count(*) FROM events where workflow like '%Get-cpu' and DATE(date_ts) >= '2019-01-20' - INTERVAL 2 WEEK group by WEEK(date_ts,1) order by date_ts LIMIT 3 "
            ]

            workflow_month = [
            "SELECT MONTH(date_ts),count(*) FROM events where workflow like '%Get-Size' and DATE(date_ts) >= '2019-01-20' - INTERVAL 2 MONTH group by MONTH(date_ts) order by date_ts LIMIT 3"
            ,"SELECT MONTH(date_ts),count(*) FROM events where workflow like '%restart-service' and DATE(date_ts) >= '2019-01-20' - INTERVAL 2 MONTH group by MONTH(date_ts) order by date_ts LIMIT 3"
            ,"SELECT MONTH(date_ts),count(*) FROM events where workflow like '%Restart-Server' and DATE(date_ts) >= '2019-01-20' - INTERVAL 2 MONTH group by MONTH(date_ts) order by date_ts LIMIT 3"
            ,"SELECT MONTH(date_ts),count(*) FROM events where workflow like '%Restart-WinRM' and DATE(date_ts) >= '2019-01-20' - INTERVAL 2 MONTH group by MONTH(date_ts) order by date_ts LIMIT 3"
            ,"SELECT MONTH(date_ts),count(*) FROM events where workflow like '%Get-Ram' and DATE(date_ts) >= '2019-01-20' - INTERVAL 2 MONTH group by MONTH(date_ts) order by date_ts LIMIT 3"
            ,"SELECT MONTH(date_ts),count(*) FROM events where workflow like '%Get-cpu' and DATE(date_ts) >= '2019-01-20' - INTERVAL 2 MONTH group by MONTH(date_ts) order by date_ts LIMIT 3"
            ]

            workflow_year = [
            "SELECT YEAR('2019-01-20'),count(*) FROM events where workflow like '%Get-Size' and DATE(date_ts) >= DATE('2019-01-20'-INTERVAL 1 YEAR)"
            ,"SELECT YEAR('2019-01-20'),count(*) FROM events where workflow like '%Restart-Service' and DATE(date_ts) >= DATE('2019-01-20'-INTERVAL 1 YEAR)"
            ,"SELECT YEAR('2019-01-20'),count(*) FROM events where workflow like '%Restart-Server' and DATE(date_ts) >= DATE('2019-01-20'-INTERVAL 1 YEAR)"
            ,"SELECT YEAR('2019-01-20'),count(*) FROM events where workflow like '%Restart-WinRM' and DATE(date_ts) >= DATE('2019-01-20'-INTERVAL 1 YEAR)"
            ,"SELECT YEAR('2019-01-20'),count(*) FROM events where workflow like '%Get-RAM' and DATE(date_ts) >= DATE('2019-01-20'-INTERVAL 1 YEAR)"
            ,"SELECT YEAR('2019-01-20'),count(*) FROM events where workflow like '%Get-CPU' and DATE(date_ts) >= DATE('2019-01-20'-INTERVAL 1 YEAR)"
            ]

            workflow_query = [workflow_day,workflow_week,workflow_month,workflow_year]
            t_res = []
            workflow_cnt2 = dict()
            workflow_pct2 = dict()
            key2 = ['day','week','month','year']
            total_workflow_cnt2 = 0 

            for j in range(0,4):
                queries = workflow_query[j]
                tab = []
                for i in range(0,6):
                    querie = queries[i]
                    cursor.execute(querie)
                    res_row = cursor.fetchall()
                    t = []
                    if j == 0: #day
                        t = utils.correctArray(res_row,2)
                    elif j == 1:#week
                        t = utils.correctArray(res_row,0)
                    elif j == 2:#month
                        t = utils.correctArray(res_row,1)
                    elif j == 3:#year
                       t = utils.correctArray(res_row,3)
                    print(t)
                    t = [int(a) for a in t]
                    rows = cursor.fetchall()
                    tab.append(t)
                    if i== 0:
                        getsize_cnt2 = t[-1]
                    if i== 1:    
                        service_cnt2 = t[-1]
                    if i== 2:
                        server_cnt2 = t[-1]
                    if i== 3:
                        ticket_cnt2 = t[-1]
                    if i== 4:
                        RAM_cnt2 = t[-1]
                    if i== 5:
                        CPU_cnt2 = t[-1]

                total_workflow_cnt2 = getsize_cnt2 + service_cnt2 + server_cnt2 + ticket_cnt2 + RAM_cnt2 + CPU_cnt2
                workflow_cnt2[key2[j]] = [getsize_cnt2,service_cnt2,server_cnt2,ticket_cnt2,RAM_cnt2,CPU_cnt2]
                workflow_pct2[key2[j]] = {'getsize_pct':"{0:.0f}%".format(getsize_cnt2/total_workflow_cnt2 * 100),'service_pct':"{0:.0f}%".format(service_cnt2/total_workflow_cnt2 * 100),'server_pct':"{0:.0f}%".format(server_cnt2/total_workflow_cnt2 * 100),'ticket_pct': "{0:.0f}%".format(ticket_cnt2/total_workflow_cnt2 * 100),'RAM_pct': "{0:.0f}%".format(RAM_cnt2/total_workflow_cnt2 * 100),'CPU_pct': "{0:.0f}%".format(CPU_cnt2/total_workflow_cnt2 * 100)} if total_workflow_cnt2 !=0 else {'getsize_pct2':'0','service_cnt2':'0','server_pct2':'0','ticket_pct2':'0','RAM_pct2':'0','CPU_pct2':'0'}

                tot = [sum(x) for x in zip(*tab)]
                tab.append(tot)
                t_res.append(tab)

        finally:
            conn.close()

        print(total_workflow_cnt2)
        print(workflow_cnt2)
        print(workflow_pct2)    

        #hardcoding context for demo
        '''workflow_cnt2 = {'week': [10, 2, 38, 11, 14, 4], 'year': [1062, 156, 2910, 673, 301, 401], 'day': [3, 0, 0, 1, 0, 0], 'month': [16, 2, 45, 12, 18, 7]}
        workflow_pct2 = {'week': {'CPU_pct': '0%', 'ticket_pct': '0%', 'service_pct': '0%', 'getsize_pct': '0%', 'RAM_pct': '0%', 'server_pct': '0%'}, 'year': {'CPU_pct': '0%', 'ticket_pct': '0%', 'service_pct': '0%', 'getsize_pct': '0%', 'RAM_pct': '0%', 'server_pct': '0%'}, 'day': {'CPU_pct': '0%', 'ticket_pct': '0%', 'service_pct': '0%', 'getsize_pct': '0%', 'RAM_pct': '0%', 'server_pct': '0%'}, 'month': {'CPU_pct': '0%', 'ticket_pct': '0%', 'service_pct': '0%', 'getsize_pct': '0%', 'RAM_pct': '0%', 'server_pct': '0%'}}
        t_res = [[[2, 1, 3, 2, 0, 5, 3], [0, 0, 0, 0, 1, 1, 0], [4, 0, 3, 27, 4, 7, 0], [0, 1, 0, 9, 0, 1, 1], [0, 4, 0, 2, 5, 7, 0], [1, 1, 1, 0, 0, 4, 0], [7, 7, 7, 40, 10, 25, 4]], [[27, 19, 10], [5, 2, 2], [42, 81, 38], [27, 86, 11], [2, 11, 14], [4, 8, 4], [107, 207, 79]], [[180, 114, 16], [16, 14, 2], [283, 355, 45], [46, 151, 12], [26, 15, 18], [35, 33, 7], [586, 682, 100]], [[0, 1062], [0, 156], [0, 2910], [0, 673], [0, 301], [0, 401], [0, 5503]]]'''

        context ={
            'today': datetime.datetime.today().strftime('%Y-%m-%d'),
            'workflow_cnt':workflow_cnt2,
            'workflow_pct':workflow_pct2,
            'arr_worfklow':t_res,
        }
        return render(request, 'workflow.html', context)

def server(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        conn = utils.get_autobot_connection()
        try:
            cursor = conn.cursor()
            server_query = {
                "day":"SELECT server, COUNT(*) FROM events where DATE(`Date_Autobot`) ='2019-01-20' and (ticket != 'In Maintenance' and ticket !='Under Reboot') GROUP BY server HAVING count(*) > 1 ORDER by COUNT(*) desc LIMIT 5",
                "week":"SELECT server, COUNT(*) FROM events where YEARWEEK(`Date_Autobot`) = YEARWEEK('2019-01-20') AND (ticket != 'In Maintenance' and ticket !='Under Reboot') GROUP BY server HAVING count(*) > 1 ORDER by COUNT(*) desc LIMIT 5",
                "month":"SELECT server, COUNT(*) FROM events where MONTH(`Date_Autobot`) =  MONTH('2019-01-20') AND YEAR(Date_Autobot) = YEAR('2019-01-20') and (ticket != 'In Maintenance' and ticket !='Under Reboot') GROUP BY server HAVING count(*) > 1 ORDER by COUNT(*) desc LIMIT 5",
                "year":"SELECT server, COUNT(*) FROM events where (`Date_Autobot`) >= DATE('2019-01-20'-INTERVAL 1 YEAR) and (ticket != 'In Maintenance' and ticket !='Under Reboot') GROUP BY server HAVING count(*) > 1 ORDER by COUNT(*) desc LIMIT 5"
            }
            server_cnt = dict()
            server_pct = dict()
            for key,value in server_query.items():
                cursor.execute(value)

                server_rows = cursor.fetchall()
                server_name_lst = [e[0].split('.')[0].upper() for e in server_rows]
                server_name_lst = [str(r) for r in server_name_lst] #to remove unicode in [u'yyyyy', u'xxxxx']
                server_cnt_lst = [e[1] for e in server_rows]
                server_cnt_lst = [int(a) for a in server_cnt_lst]
                server_total = sum(server_cnt_lst)
                server_pct_lst = ["{0:.0f}%".format(float(v)/float(server_total) * 100) for v in server_cnt_lst]
                server_cnt[key] = {'name':server_name_lst,'cnt':server_cnt_lst}
                server_pct[key] = server_pct_lst

            print("server_cnt:%s"%server_cnt)
            print(server_pct)
        finally:
            conn.close()
         
        #hardcoding context for demo         
        '''server_cnt = {'week': {'cnt': [44, 38, 27, 19, 18], 'name': ['AT039P-CTRL', 'AT032P', 'AT145P', 'AT052P', 'AT004P-DBA01VM01']}, 'month': {'cnt': [64, 48, 37, 19, 18], 'name': ['AT039P-CTRL', 'AT032P', 'AT145P', 'AT052P', 'AT004P-DBA01VM01']}, 'day': {'cnt': [3, 3, 2, 2], 'name': ['ATNNE6XMS3B0', 'AT032P', 'AT60XMS3B0', 'AT74XMS3B0']}, 'year': {'cnt': [3300, 2488, 1444, 403, 386], 'name': ['AT039P-CTRL', 'ATJ05XLR1B0', 'AT032P', 'AT228P', 'AT258P']}}  
        server_pct = {'week': ['30%', '26%', '18%', '13%', '12%'], 'month': ['34%', '26%', '20%', '10%', '10%'], 'day': ['30%', '30%', '20%', '20%'], 'year': ['41%', '31%', '18%', '5%', '5%']}'''       
            
        context = {
            'today': datetime.datetime.today().strftime('%Y-%m-%d'),
            'server_cnt':server_cnt,
            'server_pct':server_pct,
       }
        return render(request, 'server.html',context)

def search_ts(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        context = {
            'today': datetime.datetime.today().strftime('%Y-%m-%d'),
       }
        return render(request, 'search_ts.html',context)

def search_backup(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else: 
        conn = utils.get_autobot_connection()
        try:
            cursor = conn.cursor()
            server_query = {
                "day":"SELECT server, COUNT(*) FROM events where DATE(`Date_Autobot`) = '2019-01-20' and (ticket != 'In Maintenance' or ticket !='Under Reboot') GROUP BY server HAVING count(*) > 1 ORDER by COUNT(*) desc LIMIT 5",
                "week":"SELECT server, COUNT(*) FROM events where YEARWEEK(`Date_Autobot`,1) = YEARWEEK('2019-01-20') and (ticket != 'In Maintenance' or ticket !='Under Reboot') GROUP BY server HAVING count(*) > 1 ORDER by COUNT(*) desc LIMIT 5",
                "month":"SELECT server, COUNT(*) FROM events where MONTH(`Date_Autobot`) =  MONTH('2019-01-20') and (ticket != 'In Maintenance' or ticket !='Under Reboot') GROUP BY server HAVING count(*) > 1 ORDER by COUNT(*) desc LIMIT 5",
                "year":"SELECT server, COUNT(*) FROM events where YEAR(`Date_Autobot`) = YEAR('2019-01-20') and (ticket != 'In Maintenance' or ticket !='Under Reboot') GROUP BY server HAVING count(*) > 1 ORDER by COUNT(*) desc LIMIT 5"
            }
            server_cnt = dict()
            server_pct = dict()
            for key,value in server_query.items():
                cursor.execute(value)

                server_rows = cursor.fetchall()
                server_name_lst = [e[0].split('.')[0].upper() for e in server_rows]
                server_cnt_lst = [e[1] for e in server_rows]
                server_total = sum(server_cnt_lst)
                server_pct_lst = ["{0:.0f}%".format(float(v)/float(server_total) * 100) for v in server_cnt_lst]
                server_cnt[key] = {'name':server_name_lst,'cnt':server_cnt_lst}
                server_pct[key] = server_pct_lst

            print("server_cnt:%s"%server_cnt)
            print(server_pct)
        finally:
            conn.close()
        context = {
            'today': datetime.datetime.today().strftime('%Y-%m-%d'),
            'server_cnt':server_cnt,
            'server_pct':server_pct,
       }
        return render(request, 'search_backup.html',context)

def server_details(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        servername = request.GET.get('server', None)
        serverDetailsType = request.GET.get('serverDetailsType', None)
        data = utils.get_server_details(servername,serverDetailsType)

        return JsonResponse(data)

def problem_details(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        state = request.GET.get('state', None)
        problemDetailsType = request.GET.get('problemDetailsType', None)
        data = utils.get_problem_details(state,problemDetailsType)
        print("data:%s"%data)

        return JsonResponse(data)    

def top_problem_details(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        problem = request.GET.get('problem', None)
        problemDetailsType = request.GET.get('problemDetailsType', None)
        data = utils.get_top_problem_details(problem,problemDetailsType)
        print("data:%s"%data)

        return JsonResponse(data)  
    
def workflow_details(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        workflow_name = request.GET.get('workflow', None)
        date_type = request.GET.get('dateType', None)
        data = utils.get_workflow_details(workflow_name,date_type)

        return JsonResponse(data)

def dashboard_backup(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        conn = utils.get_autobot_connection()
        try:
            cursor = conn.cursor()

            backup_query = {
                "day":["select count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Sucessfully' and ev.id = hst.id and DATE(ev.DATE) = '2019-01-20'","select count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Failed' and ev.id = hst.id and DATE(ev.DATE) = '2019-01-20'","select count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Pending' and hst.id = ev.id and DATE(ev.DATE) = '2019-01-20'"],
                "week":["select count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Sucessfully' and ev.id = hst.id and YEARWEEK(ev.DATE)=YEARWEEK('2019-01-20')","select count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Failed' and ev.id = hst.id and YEARWEEK(ev.date)=YEARWEEK('2019-01-20')", "select count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Pending' and hst.id = ev.id and YEARWEEK(ev.date)=YEARWEEK('2019-01-20')"],
                "month":["select count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Sucessfully' and ev.id = hst.id and MONTH(ev.DATE) = MONTH('2019-01-20')","select count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Failed' and ev.id = hst.id and MONTH(ev.DATE) = MONTH('2019-01-20')", "select count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Pending' and hst.id = ev.id and MONTH(ev.DATE) = MONTH('2019-01-20')"],
                "year":["select count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Sucessfully' and ev.id = hst.id and ev.DATE >= DATE('2019-01-20'-INTERVAL 1 YEAR)","select count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Failed' and ev.id = hst.id and ev.DATE >= DATE('2019-01-20'-INTERVAL 1 YEAR)","select count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Pending' and hst.id = ev.id and ev.DATE >= DATE('2019-01-20'-INTERVAL 1 YEAR)"],
            }

            backup_cnt = dict()
            backup_pct = dict()
            backup_total = dict()

            for key, value in backup_query.items():

                cursor.execute(value[0])
                success_row = cursor.fetchone()
                success_cnt = success_row[0] 

                cursor.execute(value[1])
                failure_row = cursor.fetchone()
                failure_cnt = failure_row[0] 

                cursor.execute(value[2])
                inprogress_row = cursor.fetchone()
                inprogress_cnt = inprogress_row[0] 
                
                print(failure_cnt)
                print(success_cnt)
                print(inprogress_cnt)

                success_cnt = int(success_cnt)
                failure_cnt = int(failure_cnt)
                inprogress_cnt = int(inprogress_cnt)
                total_backup_cnt = failure_cnt + success_cnt + inprogress_cnt

                backup_cnt[key] = [success_cnt, failure_cnt, inprogress_cnt]
                backup_total[key] = [total_backup_cnt]

                if total_backup_cnt != 0:
                    backup_pct[key] = {"Success":"{0:.0f}%".format(float(success_cnt)/float(total_backup_cnt) * 100),"Failure":"{0:.0f}%".format(float(failure_cnt)/float(total_backup_cnt) * 100),"InProgress":"{0:.0f}%".format(float(inprogress_cnt)/float(total_backup_cnt) * 100)}

                print(backup_cnt)
                print(backup_pct)
                print(backup_total)


            quer_w = [
            "select Week(ev.DATE),count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Sucessfully' and ev.id = hst.id and DATE(ev.DATE) >= '2019-01-20' -INTERVAL 2 Week group by Week(ev.DATE); "
            ,"select Week(ev.DATE),count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Failed' and ev.id = hst.id and DATE(ev.DATE) >= '2019-01-20' -INTERVAL 2 Week group by Week(ev.DATE); "
            ,"select Week(ev.DATE),count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Pending' and ev.id = hst.id and DATE(ev.DATE) >= '2019-01-20' -INTERVAL 2 Week group by Week(ev.DATE); "
            ]
            quer_m = [
            "select Month(ev.DATE),count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Sucessfully' and ev.id = hst.id and DATE(ev.DATE) >= '2019-01-20' -INTERVAL 2 MONTH group by Month(ev.DATE); "
            ,"select Month(ev.DATE),count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Failed' and ev.id = hst.id and DATE(ev.DATE) >= '2019-01-20' -INTERVAL 2 MONTH group by Month(ev.DATE); "
            ,"select Month(ev.DATE),count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Pending' and ev.id = hst.id and DATE(ev.DATE) >= '2019-01-20' -INTERVAL 2 MONTH group by Month(ev.DATE); "
            ]
            quer_d = [
            "select DayOfYear(ev.DATE),count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Sucessfully' and ev.id = hst.id and DATE(ev.DATE) >= '2019-01-20' -INTERVAL 6 Day group by Day(ev.DATE); "
            ,"select DayOfYear(ev.DATE),count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Failed' and ev.id = hst.id and DATE(ev.DATE) >= '2019-01-20' -INTERVAL 6 Day group by Day(ev.DATE); "
            ,"select DayOfYear(ev.DATE),count(*) from history_backup hst, events_backup ev where hst.status = 'Rerun Pending' and ev.id = hst.id and DATE(ev.DATE) >= '2019-01-20' -INTERVAL 6 Day group by Day(ev.DATE); "
            ]

            tab = [quer_d,quer_w,quer_m]
            data_quer = []
            query_day = " SELECT DAYOFYEAR('2019-01-20')"
            cursor.execute(query_day)
            cday = cursor.fetchone()
            for i in range(0,3):
                queries = tab[i]
                total = []

                cursor.execute(queries[0])
                rerun_success_row = cursor.fetchall()
                rerun_success_trend = [e[1] for e in rerun_success_row]
                if i == 2 :
                    rerun_success_trend = utils.correctArray(rerun_success_row,1)
                elif i == 0:
                    rerun_success_trend = utils.correctArray(rerun_success_row,2)
                elif i == 1:
                    rerun_success_trend = utils.correctArray(rerun_success_row,0)

                cursor.execute(queries[1])
                rerun_failure_row = cursor.fetchall()
                rerun_failure_trend = [e[1] for e in rerun_failure_row]
                if i == 2 :
                    rerun_failure_trend = utils.correctArray(rerun_failure_row,1)
                elif i == 0:
                    rerun_failure_trend = utils.correctArray(rerun_failure_row,2)
                elif i == 1:
                    rerun_failure_trend = utils.correctArray(rerun_failure_row,0)

                cursor.execute(queries[2])
                rerun_progress_row = cursor.fetchall()
                rerun_progress_trend = [e[1] for e in rerun_progress_row]
                if i == 2 :
                    rerun_progress_trend = utils.correctArray(rerun_progress_row,1)
                elif i == 0:
                    rerun_progress_trend = utils.correctArray(rerun_progress_row,2)
                elif i == 1:
                    rerun_progress_trend = utils.correctArray(rerun_progress_row,0)

                for i in range(0,len(rerun_success_trend)):
                    sum = rerun_success_trend[i] + rerun_failure_trend[i] + rerun_progress_trend[i]
                    total.append(sum)

                rerun_success_trend = [int(a) for a in rerun_success_trend]
                rerun_failure_trend = [int(a) for a in rerun_failure_trend]
                rerun_progress_trend = [int(a) for a in rerun_progress_trend]
                total = [int(a) for a in total]               
                res = [rerun_success_trend,rerun_failure_trend,rerun_progress_trend,total]
                data_quer.append(res)

        finally:
            conn.close()
            
        #hardcoding context for demo
        '''backup_cnt = {'week': [0, 0, 0], 'month': [143, 66, 19], 'day': [0, 0, 0], 'year': [2806, 1601, 203]}
        backup_pct = {'month': {'Failure': '29%', 'InProgress': '8%', 'Success': '63%'}, 'year': {'Failure': '35%', 'InProgress': '4%', 'Success': '61%'}}
        backup_total = {'week': [0], 'month': [228], 'day': [0], 'year': [4610]}
        data_quer = [[[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0]], [[4, 0, 0], [105, 0, 0], [0, 0, 0], [109, 0, 0]], [[121, 122, 0], [67, 226, 0], [1, 7, 0], [189, 355, 0]]]'''
            
        context = {
            'today': datetime.datetime.today().strftime('%Y-%m-%d'),
            'backup_cnt':backup_cnt,
            'backup_pct':backup_pct,
            'backup_total':backup_total,
            'data_quer':data_quer,
        }
        return render(request, 'dashboard_backup.html',context)

def server_backup(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        conn = utils.get_autobot_connection()
        try:
            cursor = conn.cursor()

            backup_query = {
                "week":"select ev.server as Server, count(*) from history_backup hst, events_backup ev where ev.id = hst.id and hst.status = 'Rerun Failed' and YEARWEEK(ev.date) = YEARWEEK('2019-01-20') GROUP BY server HAVING count(*) > 0 ORDER by COUNT(*) desc LIMIT 5",
                "month":"select ev.server as Server, count(*) from history_backup hst, events_backup ev where ev.id = hst.id and hst.status = 'Rerun Failed' and MONTH(ev.DATE) = MONTH('2019-01-20') AND YEAR(ev.DATE) = YEAR('2019-01-20') GROUP BY server HAVING count(*) > 0 ORDER by COUNT(*) desc LIMIT 5",
                "year":"select ev.server as Server, count(*) from history_backup hst, events_backup ev where ev.id = hst.id and hst.status = 'Rerun Failed' and ev.DATE >= DATE('2019-01-20'-INTERVAL 1 YEAR) GROUP BY server HAVING count(*) > 0 ORDER by COUNT(*) desc LIMIT 5",
            }

            server_cnt = dict()
            server_pct = dict()
            for key,value in backup_query.items():
                cursor.execute(value)

                server_rows = cursor.fetchall()
                server_name_lst = [e[0].split('.')[0].upper() for e in server_rows]
                server_name_lst = [str(r) for r in server_name_lst] #to remove unicode in [u'yyyyy', u'xxxxx']

                server_cnt_lst = [e[1] for e in server_rows]
                server_cnt_lst = [int(a) for a in server_cnt_lst]
                server_total = sum(server_cnt_lst)
                server_pct_lst = ["{0:.0f}%".format(float(v)/float(server_total) * 100) for v in server_cnt_lst]
                server_cnt[key] = {'name':server_name_lst,'cnt':server_cnt_lst}
                server_pct[key] = server_pct_lst

            print("server_cnt:%s"%server_cnt)
            print(server_pct)

            errorcode_query = {
                "week":"select error, count(*) from events_backup where YEARWEEK(DATE) = YEARWEEK('2019-01-20') group by error ORDER by COUNT(*) desc LIMIT 5",
                "month":"select error, count(*) from events_backup where MONTH(DATE) = MONTH('2019-01-20') group by error ORDER by COUNT(*) desc LIMIT 5",
                "year":"select error, count(*) from events_backup where DATE >= DATE('2019-01-20'-INTERVAL 1 YEAR) group by error ORDER by COUNT(*) desc LIMIT 5",
            }

            errorcode_cnt = dict()
            errorcode_pct = dict()
            for key,value in errorcode_query.items():
                cursor.execute(value)

                errorcode_rows = cursor.fetchall()
                errorcode_name_lst = [e[0] for e in errorcode_rows]
                errorcode_name_lst = [str(r) for r in errorcode_name_lst] #to remove unicode in [u'yyyyy', u'xxxxx']

                errorcode_cnt_lst = [e[1] for e in errorcode_rows]
                errorcode_cnt_lst = [int(a) for a in errorcode_cnt_lst]
                errorcode_total = sum(errorcode_cnt_lst)
                errorcode_pct_lst = ["{0:.0f}%".format(float(v)/float(errorcode_total) * 100) for v in errorcode_cnt_lst]
                errorcode_cnt[key] = {'name':errorcode_name_lst,'cnt':errorcode_cnt_lst}
                errorcode_pct[key] = errorcode_pct_lst

            print("errorcode_cnt:%s"%errorcode_cnt)
            print(errorcode_pct)

        finally:
            conn.close()
        
        #hardcoding context for demo
        '''server_cnt = {'week': {'cnt': [], 'name': []}, 'month': {'cnt': [], 'name': []}, 'year': {'cnt': [47, 45, 45, 36, 34], 'name': ['AT331P', 'AT622D', 'AT165D', 'AT776D', 'AT080D']}}
        server_pct = {'week': [], 'month': [], 'year': ['23%', '22%', '22%', '17%', '16%']}
        errorcode_cnt = {'week': {'cnt': [], 'name': []}, 'month': {'cnt': [54, 53, 30, 21, 19], 'name': ['156', '4201', '1', '58', '13']}, 'year': {'cnt': [1739, 588, 493, 486, 275], 'name': ['87', '156', '1', '4201', '58']}}
        errorcode_pct = {'week': [], 'month': ['31%', '30%', '17%', '12%', '11%'], 'year': ['49%', '16%', '14%', '14%', '8%']}'''
        
        context = {
            'today': datetime.datetime.today().strftime('%Y-%m-%d'),
            'server_cnt':server_cnt,
            'server_pct':server_pct,
            'errorcode_cnt':errorcode_cnt,
            'errorcode_pct':errorcode_pct,
            }

        return render(request, 'server_backup.html',context)

def server_backup_details(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        backupserver = request.GET.get('backupserver', None)
        serverDetailsType = request.GET.get('serverDetailsType', None)
        data = utils.get_server_backup_details(backupserver,serverDetailsType)

        return JsonResponse(data)

def errorcode_details(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        errorcode = request.GET.get('errorcode', None)
        errorcodeDetailsType = request.GET.get('errorcodeDetailsType', None)
        data = utils.get_errorcode_details(errorcode,errorcodeDetailsType)

        return JsonResponse(data)
    
def backup_failure_details(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        status = request.GET.get('status', None)
        DetailsType = request.GET.get('DetailsType', None)
        data = utils.get_backup_failure_details(status,DetailsType)

        return JsonResponse(data)

def ts_ticket_details(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        type = request.GET.get('type', None)
        DetailsType = request.GET.get('DetailsType', None)
        data = utils.get_ts_ticket_details(type,DetailsType)

        return JsonResponse(data)
    
def ts_alert_details(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        type = request.GET.get('type', None)
        DetailsType = request.GET.get('DetailsType', None)
        data = utils.get_ts_alert_details(type,DetailsType)
        #import pdb;pdb.set_trace()
        return JsonResponse(data)

def ts_action_details(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        myDate = request.GET.get('myDate', None)
        data = utils.get_ts_action_details(myDate)

        return JsonResponse(data)

def history_details(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        historyID = request.GET.get('historyID', None)
        data = utils.get_history_details(historyID)

        return JsonResponse(data)

def history_backup_details(request):
        historyID = request.GET.get('historyID', None)
        data = utils.get_history_backup_details(historyID)

        return JsonResponse(data)

def ts_top_action_details(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        type = request.GET.get('type', None)
        data = utils.get_ts_top_action_details(type)

        return JsonResponse(data)

def search_ts_details(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        server = request.GET.get('server', None)
        start_date = request.GET.get('start-date', None)
        end_date = request.GET.get('end-date', None)
        conn = utils.get_autobot_connection()
        try:
            cursor = conn.cursor()
            server_details_query = "SELECT server, date_ts, object, message, workflow, ticket, ticket, id FROM events WHERE DATE(date_ts) >= '{1}' and  DATE(date_ts) <= '{2}' and (server like '%{0}%' OR ticket like '%{0}%')"

            cursor.execute(server_details_query.format(server,start_date,end_date))

            server_details_rows = [list(e) for e in cursor.fetchall()]

            print(server_details_rows)
            for elem in server_details_rows :
                elem[4] = elem[4].split('\\')[-1].title()
                elem[0] = elem[0].split('.')[0].upper()
                if "@" in elem[2]:
                    elem[2] = elem[2].split('@')[1]
                if len(elem[2])>80 :
                    elem_first = (elem[2])[:50] 
                    elem_last = (elem[2])[-20:]
                    elem[2] = elem_first + "....." + elem_last

            data = {'server_details':server_details_rows}
            print(server_details_rows)
        finally:
            conn.close()

        return JsonResponse(data)


def search_backup_details(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        server = request.GET.get('server', None)
        start_date = request.GET.get('start-date', None)
        end_date = request.GET.get('end-date', None)
        conn = utils.get_autobot_connection()
        try:
            cursor = conn.cursor()
            backup_details_query = "Select ev.server, ev.date, hst.status, hst2.status, ev.id from history_backup hst, history_backup hst2, events_backup ev where  (ev.server like '%{0}%' OR ev.ticket like '%{0}%') and ev.date >= '{1}' and ev.date <= '{2}' and ev.id = hst.id and hst.action = 'Backup Rerun' and hst2.id = ev.id and hst2.action = 'Incident Ticket Created'"

            cursor.execute(backup_details_query.format(server,start_date,end_date))

            backup_details_rows = [list(e) for e in cursor.fetchall()]

            print(backup_details_rows)

            for elem in backup_details_rows :
                elem[0] = elem[0].split('.')[0].upper()
            data = {'server_details':backup_details_rows}
            print(backup_details_rows)
        finally:
            conn.close()

        return JsonResponse(data)

def dashboard_problem(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        conn = utils.get_autobot_connection()
        conn_deis = utils.get_deis_connection()

        try:
            cursor = conn.cursor()
            cursor_deis = conn_deis.cursor()

            problem_query = {
                    "week":["Select count(distinct hst.problem) as cnt from history_problem hst, history_problem hst2 where hst.action = 'Problem Creation' and hst.problem = hst2.problem and hst2.action = 'Ticket linked to problem' and YEARWEEK(hst.date,1) = YEARWEEK('2019-01-20')","Select count(distinct pb.sequence) from _SMDBA_._PROBMGMT_ pb, _SMDBA_._TELMASTE_ inc where pb.state ='C' and pb.[SEQ_SUPPORTSTAF] = (select sequence from _SMDBA_._PERSONNEL_ where CODE = 'AUTOBOT') and inc.[SEQ_PM:] = pb.sequence and datepart (wk, DATE_CLOSED) = datepart ( wk, getdate() )"],
                    "month":["Select count(distinct hst.problem) as cnt from history_problem hst, history_problem hst2 where hst.action = 'Problem Creation' and hst.problem = hst2.problem and hst2.action = 'Ticket linked to problem' and MONTH(hst.DATE) = MONTH('2019-01-20')","Select count(distinct pb.sequence) from _SMDBA_._PROBMGMT_ pb, _SMDBA_._TELMASTE_ inc where pb.state ='C' and pb.[SEQ_SUPPORTSTAF] = (select sequence from _SMDBA_._PERSONNEL_ where CODE = 'AUTOBOT') and inc.[SEQ_PM:] = pb.sequence and datepart (mm, DATE_CLOSED) = datepart ( mm, getdate() )"],
                    "year":["Select count(distinct hst.problem) as cnt from history_problem hst, history_problem hst2 where hst.action = 'Problem Creation' and hst.problem = hst2.problem and hst2.action = 'Ticket linked to problem' and YEAR(hst.DATE) = YEAR('2019-01-20')","Select count(distinct pb.sequence) from _SMDBA_._PROBMGMT_ pb, _SMDBA_._TELMASTE_ inc where pb.state ='C' and pb.[SEQ_SUPPORTSTAF] = (select sequence from _SMDBA_._PERSONNEL_ where CODE = 'AUTOBOT') and inc.[SEQ_PM:] = pb.sequence and datepart (yy, DATE_CLOSED) = datepart ( yy, getdate() ) and [DATE_OPEN] >= '2017-07-13'"],
                }

            problem_cnt = dict()

            for key,value in problem_query.items():

                cursor.execute(value[0])
                cursor_deis.execute(value[1])

                open_problem_rows = cursor.fetchone()
                closed_problem_rows = cursor_deis.fetchone()

                open_problem_cnt = open_problem_rows[0]
                closed_problem_cnt = closed_problem_rows[0]

                print(open_problem_cnt)
                print(closed_problem_cnt)

                problem_cnt[key] = [open_problem_cnt,closed_problem_cnt]

            top_problem_query = {
                    "week":"Select server, count(status) from history_problem where action = 'Ticket linked to problem' and YEARWEEK(date,1) = YEARWEEK('2019-01-20') group by problem order by count(status) desc LIMIT 5",
                    "month":"Select server, count(status) from history_problem where action = 'Ticket linked to problem' and MONTH(DATE) = MONTH('2019-01-20') group by problem order by count(status) desc LIMIT 5",
                    "year":"Select server, count(status) from history_problem where action = 'Ticket linked to problem' and YEAR(DATE) = YEAR('2019-01-20') group by problem order by count(status) desc LIMIT 5",
                }

            top_problem_cnt = dict()
            for key,value in top_problem_query.items():
                cursor.execute(value)

                top_problem_rows = cursor.fetchall()
                top_problem_name_lst = [e[0] for e in top_problem_rows]
                top_problem_cnt_lst = [e[1] for e in top_problem_rows]

                top_problem_cnt[key] = {'name': top_problem_name_lst,'cnt': top_problem_cnt_lst}

            print("top_problem_cnt:%s"% top_problem_cnt)

        finally:
            conn.close()
            conn_deis.close()

        context = {
            'today': datetime.datetime.today().strftime('%Y-%m-%d'),
            'top_problem_cnt': top_problem_cnt,
            'problem_cnt': problem_cnt,
       }
        import pdb;pdb.set_trace()
        return render(request, 'dashboard_problem.html',context)

def index(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        context = {
            'today': datetime.datetime.today().strftime('%Y-%m-%d'),
        }
        return render(request, 'index.html',context)
def admin_skype_bot_ia(request):
    if "admin" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'')
    else:
        context = {
            'today': datetime.datetime.today().strftime('%Y-%m-%d'),
        }
        return render(request, 'admin_skype_bot_ia.html',context)
def admin_skype_bot_ia_data(request):
    if "admin" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'')
    else:
        conn = get_autobot_dev_connection()
        try:
            cursor = conn.cursor()
            skype_bot_ia = "SELECT * FROM skype_bot_ia"

            cursor.execute(skype_bot_ia)

            result = [list(e) for e in cursor.fetchall()]
            
            list_types = "SELECT DISTINCT Type FROM skype_bot_ia"
            
            cursor.execute(list_types)
            
            list_types = cursor.fetchall()
            
        finally:
            conn.close()
            data = {
                'skype_bot_ia_row': result,
                'list_types': list_types,
            }
            return JsonResponse(data)

def table_crt(request):
    if "admin" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'')
    else:            
        context = {
            'today': datetime.datetime.today().strftime('%Y-%m-%d'),
        }
        return render(request, 'table_crt.html',context)
    
def table_crt_data(request):
    if "admin" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'')
    else:
        conn = get_autobot_dev_connection()
        try:
            cursor = conn.cursor()
            grp_crt = "SELECT * FROM grp_crt"

            cursor.execute(grp_crt)

            result = [list(e) for e in cursor.fetchall()]
            
            list_group = "SELECT DISTINCT Groupe FROM grp_crt"
            
            cursor.execute(list_group)
            
            list_group = cursor.fetchall()
            
            list_category = "SELECT DISTINCT Category FROM grp_crt"
            
            cursor.execute(list_category)
            
            list_category_data = cursor.fetchall()
            
            list_criticity = "SELECT DISTINCT Criticity FROM grp_crt"
            
            cursor.execute(list_criticity)
            
            list_criticity_data = cursor.fetchall()
            
        finally:
            conn.close()
            data = {
                'table_crt': result,
                'list_group': list_group,
                'list_category': list_category_data,
                'list_criticity': list_criticity_data
            }
            return JsonResponse(data)

def replace(text):
    return text.replace("'","\\'").replace('"','\\"').replace("&","\\&")

def add_bdd(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        type_page = request.GET.get('type',None)
            
        conn = get_autobot_dev_connection()
        try:
            cursor = conn.cursor()
            
            if type_page == "grp_crt":    
                message = replace(request.GET.get('message', None))
                obj = replace(request.GET.get('object', None))
                group = request.GET.get('group', None)
                criticity = request.GET.get('criticity', None)
                category = request.GET.get('category', None)
                hostname = replace(request.GET.get('hostname', None))
                weight = request.GET.get('weight', None)

                request = "INSERT INTO grp_crt (Message,Object,Groupe,Criticity,Category,Hostname,Weight) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')"
                cursor.execute(request.format(message,obj,group,criticity,category,hostname,weight))
            
            elif type_page == "skype_bot_ia":
                sentence = replace(request.GET.get('sentence', None))
                argument1 = replace(request.GET.get('argument1', None))
                argument2 = replace(request.GET.get('argument2', None))
                action = replace(request.GET.get('action', None))
                response = replace(request.GET.get('response', None))
                validation = request.GET.get('validation', None)
                type = request.GET.get('type_ia', None)

                request = "INSERT INTO skype_bot_ia (Sentence,arg1,arg2,action,reponse,validation,type) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')"
                cursor.execute(request.format(sentence,argument1,argument2,action,response,validation,type))
            
        finally:
            conn.close()
        return JsonResponse("Success",safe=False)
    
def edit_bdd(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        type_page = request.GET.get('type',None)
        conn = get_autobot_dev_connection()
        try:
            cursor = conn.cursor()
            
            if type_page == "grp_crt":   
                my_id = request.GET.get('my_id', None)
                message = replace(request.GET.get('message', None))
                obj = replace(request.GET.get('object', None))
                group = request.GET.get('group', None)
                criticity = request.GET.get('criticity', None)
                category = request.GET.get('category', None)
                hostname = replace(request.GET.get('hostname', None))
                weight = request.GET.get('weight', None)

                request = "UPDATE grp_crt SET message = '{1}', object = '{2}', groupe = '{3}', criticity = '{4}', category = '{5}', hostname = '{6}', weight = '{7}' WHERE id = '{0}'"
                cursor.execute(request.format(my_id,message,obj,group,criticity,category,hostname,weight))
                
            elif type_page == "skype_bot_ia":
                my_id = request.GET.get('my_id', None)
                sentence = replace(request.GET.get('sentence', None))
                argument1 = replace(request.GET.get('argument1', None))
                argument2 = replace(request.GET.get('argument2', None))
                action = replace(request.GET.get('action', None))
                response = replace(request.GET.get('response', None))
                validation = request.GET.get('validation', None)
                type = request.GET.get('type_ia', None)
                
                if type == "": #for update to Null in bdd
                    request = "UPDATE skype_bot_ia SET Sentence = '{1}', arg1 = '{2}', arg2 = '{3}', Action = '{4}', reponse = '{5}', Validation = '{6}', Type = NULL WHERE ID = '{0}'"
                    cursor.execute(request.format(my_id,sentence,argument1,argument2,action,response,validation,type))
                else:
                    request = "UPDATE skype_bot_ia SET Sentence = '{1}', arg1 = '{2}', arg2 = '{3}', Action = '{4}', reponse = '{5}', Validation = '{6}', Type = '{7}' WHERE ID = '{0}'"
                    cursor.execute(request.format(my_id,sentence,argument1,argument2,action,response,validation,type))
                
        finally:
            conn.close()
            return JsonResponse("Success",safe=False)
            
def delete_bdd(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        type_page = request.GET.get('type',None)
        conn = get_autobot_dev_connection()
        
        try:
            cursor = conn.cursor()
            
            if type_page == "grp_crt":   
                id_delete = request.GET.get('id',None)
                request = "DELETE FROM grp_crt WHERE id = '{0}'"
                cursor.execute(request.format(id_delete))
                
            if type_page == "skype_bot_ia":
                id_delete = request.GET.get('id',None)
                request = "DELETE FROM skype_bot_ia WHERE id = '{0}'"
                cursor.execute(request.format(id_delete))
            
        finally:
                conn.close()
                return JsonResponse("Delete",safe=False)
        
def login(request):

    context = {
        'today': datetime.datetime.today().strftime('%Y-%m-%d'),
   }
   
    if request.method == 'GET':
        return render(request, 'login.html',context)
    #if request.method == 'POST':

def get_bind_user(user):
    """ This method returns the bind user string for the user"""
    user_dn = settings.AD_DN
    login_attr = '(%s=%s)' % (settings.AD_LOGIN_ATTR,user)
    attr_search = settings.AD_ATTR_SEARCH
    
    ad_server = settings.AD_URL
    
    conn = ldap.initialize(settings.AD_URL)
    conn.set_option(ldap.OPT_REFERRALS,0)
    conn.set_option(ldap.OPT_PROTOCOL_VERSION, settings.LDAP_PROTOCOL_VERSION)

    try:
        conn.bind(settings.AD_USER ,settings.AD_PASSWORD)
        conn.result()
    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        return []


    try:
        result = conn.search_s(user_dn,
                               ldap.SCOPE_SUBTREE,
                               login_attr, attr_search)
    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        return []
    
     # Check Groups
    try:
        cn_user = result[0][1]['cn'][0]
    except:
        return []
    
    result[0][1]['role'] = ''
    
    
    # Check if Security user
    try:
        # this returns the groups!
        autobot_user_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.AUTOBOT_GRP_DN, ['cn',])
        autobot_group = [secug[0] for secug in autobot_user_group]

        if any(cn_user.decode('utf-8') in secu for secu in autobot_group):
            result[0][1]['role']= 'user'
            
    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        
    try:
        # this returns the groups!
        security_user_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.SECU_GRP_DN, ['cn',])
        security_group = [secug[0] for secug in security_user_group]

        if any(cn_user.decode('utf-8') in secu for secu in security_group):
            result[0][1]['role']= 'user'

    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        
        
    try:
        # this returns the groups!
        admin_user_group = conn.search_s(user_dn, ldap.SCOPE_SUBTREE, settings.ADMIN_GRP_DN, ['cn',])
        admin_group = [secug[0] for secug in admin_user_group]

        if any(cn_user.decode('utf-8') in secu for secu in admin_group):
            result[0][1]['role']= 'admin'
            
    except:
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()

    return result[0][1]

    
def verif_login(request):
    username = request.POST.get('username', False)
    password = request.POST.get('password', False)
    print(username)

    #data = get_bind_user(username)
    #data = {'username' : 'admin', 'password' : 'admin','email' : 'nomail@neuornes-it.asia', 'role' : 'admin', 'phone' : '12345678'}
    data = {'email' : 'nomail@neuornes-it.asia', 'role' : 'admin', 'phone' : '12345678'}
    data['username'] = username
    data['password'] = password
    
    if data['username'] != 'admin' or data['password'] != 'admin':
        messages.warning(request, 'Please correct the error above.')
        #return None
        return redirect('http://185.165.144.116:'+settings.PORT+'/login?error_usr')

    try:     
        if data['role'] == 'admin' and data['username'] == 'admin' and data['password'] == 'admin': 
            request.session['token'] = True
            request.session['admin'] = True
            request.session.set_expiry(0) 
            
            context = {
                'isAdmin': 1,
                }
            print('return index.html')
            return render(request, 'index.html',context)
        
        elif data['role'] == 'user' and data['username'] == 'admin' and data['password'] == 'admin': 
            request.session['token'] = True
            request.session.set_expiry(0)
            context = {
                'isAdmin': 0,
                }
            return render(request, 'index.html',context)
        
        else:
            return redirect('http://185.165.144.116:'+settings.PORT+'/login?error_usr')
    except ():
        #return None
        return redirect('http://185.165.144.116:'+settings.PORT+'/login?error_usr')

def logout(request):
    del request.session['token']
    print("IN LOGOUT FUNCTION")
    return redirect('http://185.165.144.116:'+settings.PORT+'/login')
def personalassistant_dashboard(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        conn = get_autobot_connection()

        len_data = 0
        try:
            cursor = conn.cursor()
            personal_assistant_query = "SELECT concat(MONTHNAME(date), ' ', DAY(date)), count(*) from skype_bot where date >= ('2019-01-20' - INTERVAL 15 DAY) group by date(date)"

            cursor.execute(personal_assistant_query)
            personal_assistant_date = [list(e)[0] for e in cursor.fetchall()]

            cursor.execute(personal_assistant_query)
            personal_assistant_data = [list(e)[1] for e in cursor.fetchall()]

            len_data = len(personal_assistant_date)

            date_cnt = ''
            data_cnt = ''

            for j in range(0,len_data):
                if date_cnt != '':
                    date_cnt = date_cnt + (",") + ("'") + str(personal_assistant_date[j]) + ("'")
                else:
                    date_cnt = ("'") + str(personal_assistant_date[j]) + ("'")

            for j in range(0,len_data):
                if data_cnt != '':
                    data_cnt = data_cnt + (",") + str(personal_assistant_data[j])
                else:
                    data_cnt = str(personal_assistant_data[j])

            personal_assistant_query = "select action, count(action) as top from skype_bot where date >= ('2019-01-20' - INTERVAL 15 DAY) group by action order by top desc LIMIT 5 "
            cursor.execute(personal_assistant_query)

            personal_assistant_actions = [list(e)[0] for e in cursor.fetchall()]  
            personal_assistant_actions = [str(r) for r in personal_assistant_actions] #to remove unicode in [u'yyyyy', u'xxxxx']            

            cursor.execute(personal_assistant_query)

            personal_assistant_count = [list(e)[1] for e in cursor.fetchall()]
            personal_assistant_count = [int(a) for a in personal_assistant_count]

            len_data = len(personal_assistant_actions)

        finally:
            conn.close()

        context = {
            'today': datetime.datetime.today().strftime('%Y-%m-%d'),
            'date_cnt':date_cnt,
            'data_cnt':data_cnt,
            'actions_cnt':personal_assistant_actions,
            'sum_cnt':personal_assistant_count
        }

        #import pdb; pdb.set_trace()
        return render(request, 'personalassistant_dashboard.html',context)

def search_assistant(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        context = {
            'today': datetime.datetime.today().strftime('%Y-%m-%d'),
       }
        return render(request, 'search_assistant.html',context)
        
def search_assistant_details(request):
    if "token" not in request.session:
        return redirect('http://185.165.144.116:'+settings.PORT+'/login')
    else:
        search = request.GET.get('search', None)        
        start_date = request.GET.get('start-date', None)
        end_date = request.GET.get('end-date', None)
        
        conn = utils.get_autobot_connection()
        try:
            cursor = conn.cursor()

            details_query = "SELECT * FROM skype_bot WHERE DATE(date) >= '{0}' AND  DATE(date) <= '{1}' AND (user like '%{2}%' OR object like '%{2}%' OR action like '%{2}%')"

            cursor.execute(details_query.format(start_date,end_date,search))

            details_rows = [list(e) for e in cursor.fetchall()]

            data = {'details':details_rows}

        finally:
            conn.close()
        return JsonResponse(data)
def nothing():
    pass
