$(function(){
  'use strict';

  //draw workflow trend charts 
  
    var labels = ['May','June','July'];


 
    var options = {
        maintainAspectRatio: false,
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                display: false,
                barPercentage: 0.6,
            }],
            yAxes: [{
                display: false,
            }]
        },

    };
    var data = {
        labels: labels,
        datasets: [
            {
                label: 'Count',
                backgroundColor: 'rgba(255,255,255,.3)',
                data: cpuUsageTrd 
            },
        ]
    };
    var ctx = $('#cpuUsageTrend');
    var cpuUsageTrend = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: options
    });

    var options = {
    maintainAspectRatio: false,
    legend: {
      display: false
    },
    scales: {
      xAxes: [{
        display: false
      }],
      yAxes: [{
        display: false
      }],
    },
    elements: {
      line: {
        borderWidth: 2
      },
      point: {
        radius: 0,
        hitRadius: 10,
        hoverRadius: 4,
      },
    }
  };
  var data = {
    labels: labels,
    datasets: [
      {
        label: 'Count',
        backgroundColor: 'rgba(255,255,255,.3)',
        borderColor: 'rgba(255,255,255,.55)',
        data: serviceRestartTrd
      },
    ]
  };
  var ctx = $('#serviceRestartTrend');
  var serviceRestartTrend = new Chart(ctx, {
    type: 'line',
    data: data,
    options: options
  });  
  
  var data = {
    labels: labels,
    datasets: [
      {
        label: 'Count',
        backgroundColor: 'rgba(255,255,255,.3)',
        borderColor: 'rgba(255,255,255,.55)',
        data: diskSpaceTrd
      },
    ]
  };
  var options = {
    maintainAspectRatio: false,
    legend: {
      display: false
    },
    scales: {
      xAxes: [{
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent'
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent',
        }

      }],
      yAxes: [{
        display: false,
        ticks: {
          display: false,
          min: Math.min.apply(Math, data.datasets[0].data) - 5,
          max: Math.max.apply(Math, data.datasets[0].data) + 5,
        }
      }],
    },
    elements: {
      line: {
        borderWidth: 1
      },
      point: {
        radius: 4,
        hitRadius: 10,
        hoverRadius: 4,
      },
    }
  };
  var ctx = $('#diskSpaceTrend');
  var diskSpaceTrend = new Chart(ctx, {
    type: 'line',
    data: data,
    options: options
  });

  var data = {
    labels: labels,
    datasets: [
      {
        label: 'Count',
        backgroundColor: 'rgba(255,255,255,.3)',
        borderColor: 'rgba(255,255,255,.55)',
        data: ramUsageTrd
      },
    ]
  };
  var options = {
    maintainAspectRatio: false,
    legend: {
      display: false
    },
    scales: {
      xAxes: [{
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent'
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent',
        }

      }],
      yAxes: [{
        display: false,
        ticks: {
          display: false,
          min: Math.min.apply(Math, data.datasets[0].data) - 5,
          max: Math.max.apply(Math, data.datasets[0].data) + 5,
        }
      }],
    },
    elements: {
      line: {
        tension: 0.00001,
        borderWidth: 1
      },
      point: {
        radius: 4,
        hitRadius: 10,
        hoverRadius: 4,
      },
    }
  };
  var ctx = $('#ramUseageTrend');
  var ramUsageTrend = new Chart(ctx, {
    type: 'line',
    data: data,
    options: options
  });
  
    var options = {
        maintainAspectRatio: false,
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                display: false,
                barPercentage: 0.6,
            }],
            yAxes: [{
                display: false,
            }]
        },

    };
    var data = {
        labels: labels,
        datasets: [
            {
                label: 'Count',
                backgroundColor: 'rgba(255,255,255,.3)',
                data: osHCcCT 
            },
        ]
    };
    var ctx = $('#osHealthCheckTrend');
    var osHealthCheckTrend = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: options
    });

  var data = {
    labels: labels,
    datasets: [
      {
        label: 'Count',
        backgroundColor: 'rgba(255,255,255,.3)',
        borderColor: 'rgba(255,255,255,.55)',
        data: createTicketTrd
      },
    ]
  };
  var options = {
    maintainAspectRatio: false,
    legend: {
      display: false
    },
    scales: {
      xAxes: [{
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent'
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent',
        }

      }],
      yAxes: [{
        display: false,
        ticks: {
          display: false,
          min: Math.min.apply(Math, data.datasets[0].data) - 5,
          max: Math.max.apply(Math, data.datasets[0].data) + 5,
        }
      }],
    },
    elements: {
      line: {
        borderWidth: 1
      },
      point: {
        radius: 4,
        hitRadius: 10,
        hoverRadius: 4,
      },
    }
  };
  var ctx = $('#createTicketTrend');
  var createTicketTrend = new Chart(ctx, {
    type: 'line',
    data: data,
    options: options
  });

});
