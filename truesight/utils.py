import MySQLdb
import pymssql
from datetime import *

current_date = '2019-01-20'
def get_autobot_connection():
    conn = MySQLdb.connect(user='root', passwd='Neurones@2018', host='10.236.0.3', db='demo_autobot', charset='utf8')
    #conn = MySQLdb.connect(user='Autobot_Restitution', password='sebrocks', host='NLVCP2008P.d1.cougar.ms.lvmh', database='Autobot')
    return conn

def get_deis_connection():
    conn_deis = MySQLdb.connect(user='root', passwd='Neurones@2018', host='10.236.0.3', db='demo_autobot', charset='utf8')
    #conn_deis = pymssql.connect(user='ORCHES_CREATION', password='ORCHES_CREATION', host='CLVCP033P-SQL:1522', database='MSMINT1PRD')
    return conn_deis

def get_server_details(servername,serverDetailsType):
    conn = get_autobot_connection()
    try:
        cursor = conn.cursor()
        server_details_query = "SELECT server, Date_TS, TS_Alert, message, Object, Ticket FROM events where {1} and (ticket != 'In Maintenance' or ticket !='Under Reboot') and server like '%{0}%'"
        
        date_range_type = 'DATE(Date_Autobot) = "{0}"'.format(current_date) if serverDetailsType =="day" else 'YEARWEEK(Date_Autobot)=YEARWEEK("{0}")'.format(current_date) if serverDetailsType =="week" else 'MONTH(Date_Autobot)=MONTH("{0}") AND YEAR(Date_Autobot) = YEAR("{0}")'.format(current_date) if serverDetailsType =="month" else 'Date_Autobot >= DATE("{0}"-INTERVAL 1 YEAR)'.format(current_date)
        
        cursor.execute(server_details_query.format(servername,date_range_type))
        
        server_details_rows = [list(e) for e in cursor.fetchall()]
    
        print(server_details_rows)
        
        # for elem in server_details_rows :
            # elem[1] = elem[1].split('\\')[-1].title()
            # elem[0] = elem[0].split('.')[0].upper()
            # if 'CLVCP' in elem[4]:
                # elem[4] = "N/A"
        data = {'server_details':server_details_rows}
        print(server_details_rows)
    finally:
        conn.close()
    return data

def get_workflow_details(workflow_name,date_type):
    workflow_dict = {'RAM': '%Get-RAM', 'CPU': '%Get-cpu', 'service_restart': '%restart-service', 'server': '%Restart-Server','winRM': '%Restart-WinRM', 'disk_space': '%Get-Size'}
    conn = get_autobot_connection()
    try:
        cursor = conn.cursor()
        workflow_details_query = "select workflow, server, date_ts, message, object, ticket from events where workflow like ('{0}') and {1} "
        date_range_type = 'DATE(`DATE_ts`) = "{0}"'.format(current_date) if date_type =="day" else 'YEARWEEK(`DATE_ts`,1)-1 =YEARWEEK("{0}")'.format(current_date) if date_type =="week" else 'MONTH(`DATE_ts`)=MONTH("{0}") and YEAR(`DATE_ts`) = YEAR("{0}")'.format(current_date) if date_type =="month" else '`DATE_ts` >= DATE({0}-INTERVAL 1 YEAR)'.format(current_date)
        workflow_name_list = "','".join(workflow_dict[workflow_name]) if isinstance(workflow_dict[workflow_name], list) else workflow_dict[workflow_name]

        print(workflow_details_query.format(workflow_name_list,date_range_type))
        cursor.execute(workflow_details_query.format(workflow_name_list,date_range_type))
        workflow_details_rows = [list(e) for e in cursor.fetchall()]
        print(workflow_details_rows)
        for elem in workflow_details_rows:
            elem[0] = elem[0].split('\\')[-1].title()
            elem[1] = elem[1].split('.')[0].upper()
        data = {'workflow_details':workflow_details_rows}
        print(workflow_details_rows)
    finally:
        conn.close()
    return data

def get_server_backup_details(backupserver,serverDetailsType):
    conn = get_autobot_connection()
    try:
        cursor = conn.cursor()
        server_backup_details_query = "Select ev.server, ev.date, ev.policy, ev.schedule, ev.error, hst.status, ev.ticket from events_backup ev, history_backup hst where ev.server like '%{0}%' and ev.id = hst.id and hst.action = 'Backup Rerun' AND {1}"
        
        date_range_type = 'YEARWEEK(ev.date)=YEARWEEK("{0}")'.format(current_date) if serverDetailsType =="week" else 'MONTH(ev.date)=MONTH("{0}") AND YEAR(ev.date) = YEAR("{0}")'.format(current_date) if serverDetailsType =="month" else 'ev.date >= DATE("{0}"-INTERVAL 1 YEAR)'.format(current_date)
        
        cursor.execute(server_backup_details_query.format(backupserver,date_range_type))
        server_backup_details_rows = [list(e) for e in cursor.fetchall()]
        
        print(server_backup_details_rows)
        for elem in server_backup_details_rows :
            elem[0] = elem[0].split('.')[0].upper()
            
        data = {'server_backup_details':server_backup_details_rows}
        print(server_backup_details_rows)
    finally:
        conn.close()
    return data
    
def get_errorcode_details(errorcode,errorcodeDetailsType):
    conn = get_autobot_connection()
    try:
        cursor = conn.cursor()
        errorcode_details_query = "Select ev.error, ev.server, ev.date, ev.policy, ev.schedule, hst.status, ev.ticket from events_backup ev, history_backup hst where ev.error = '{0}' and ev.id = hst.id and hst.action = 'Backup Rerun' AND {1}"
        
        date_range_type = 'YEARWEEK(ev.date)=YEARWEEK("{0}")'.format(current_date) if errorcodeDetailsType =="week" else 'MONTH(ev.date)=MONTH("{0}")'.format(current_date) if errorcodeDetailsType =="month" else 'ev.date >= DATE("{0}"-INTERVAL 1 YEAR)'.format(current_date)
        
        cursor.execute(errorcode_details_query.format(errorcode,date_range_type))
        errorcode_details_rows = [list(e) for e in cursor.fetchall()]
        
        print(errorcode_details_rows)
        for elem in errorcode_details_rows :
            elem[1] = elem[1].split('.')[0].upper()
            
        data = {'errorcode_details':errorcode_details_rows}
        print(errorcode_details_rows)
    finally:
        conn.close()
    return data

def get_backup_failure_details(status,detailstype):
    conn = get_autobot_connection()
    try:
        cursor = conn.cursor()
        backup_failure_details_query = "Select ev.server, ev.error, ev.date, ev.policy, ev.schedule, hst.status, ev.ticket from events_backup ev, history_backup hst where ev.id = hst.id and hst.status = '{0}' AND {1}"
        
        date_range_type = 'DATE(hst.date) = "{0}"'.format(current_date) if detailstype =="day" else 'YEARWEEK(ev.DATE)=YEARWEEK("{0}")'.format(current_date) if detailstype =="week" else 'MONTH(hst.date)=MONTH("{0}")'.format(current_date) if detailstype =="month" else 'ev.date >= DATE("{0}"-INTERVAL 1 YEAR)'.format(current_date)
        
        cursor.execute(backup_failure_details_query.format(status,date_range_type))
        backup_failure_details_rows = [list(e) for e in cursor.fetchall()]
        
        print(backup_failure_details_rows)
        for elem in backup_failure_details_rows :
            elem[0] = elem[0].split('.')[0].upper()
            
        data = {'backup_failure_details':backup_failure_details_rows}
        print(backup_failure_details_rows)
    finally:
        conn.close()
    return data

def get_ts_action_details(myDate):
    conn = get_autobot_connection()
    try:
        cursor = conn.cursor()
        action_query = "SELECT date, phrase, action, user FROM skype_bot WHERE DATE(date) = '{0}'"
        
        cursor.execute(action_query.format(myDate))
        action_rows = [list(e) for e in cursor.fetchall()]
        data = {'ts_action_details':action_rows}
    
    finally:
        conn.close()
    return data

def get_ts_top_action_details(type):
    conn = get_autobot_connection()
    try:
        cursor = conn.cursor()
        
        ts_top_action = "SELECT date, phrase, object, user as top from skype_bot WHERE action = '{0}' AND date >= ('2019-01-20' - INTERVAL 15 DAY)"
        
        cursor.execute(ts_top_action.format(type))
        ts_top_action_details = [list(e) for e in cursor.fetchall()]
        
        data = {'ts_top_action_details': ts_top_action_details}
   
    finally:
        conn.close()
    return data

def get_history_details(historyID):
    conn = get_autobot_connection()
    try:
        cursor = conn.cursor()
        
        history = "select date, action, status from history where id = '{0}'"
        
        cursor.execute(history.format(historyID))
        history_details = [list(e) for e in cursor.fetchall()]
        
        data = {'history_details': history_details}
   
    finally:
        conn.close()
    return data

def get_history_backup_details(historyID):
    conn = get_autobot_connection()
    try:
        cursor = conn.cursor()
        
        history = "select date, action, status from history_backup where id = '{0}'"
        
        cursor.execute(history.format(historyID))
        history_backup_details = [list(e) for e in cursor.fetchall()]
        
        data = {'history_backup_details': history_backup_details}
   
    finally:
        conn.close()
    return data

def get_ts_alert_details(type,detailstype):
    conn = get_autobot_connection()
    try:
        cursor = conn.cursor()
        
        ts_alert_details_query = ["SELECT Server, Date_TS, Message, Object, ticket FROM events where type = 'In Maintenance' and {0}"
        ,"SELECT Server, Date_TS, Message, Object, ticket FROM events where type = 'under reboot' and {0}"
        ,"SELECT Server, Date_TS, Message, Object, ticket FROM events where {0} and type = 'Multiple'"
        ,"SELECT Server, Date_TS, Message, Object, ticket FROM events where {0} and id not in  (select id from events where type = 'Multiple' OR type = 'MASSIVE' or type = 'In maintenance' or type = 'Under reboot')"
        ,"SELECT Server, Date_TS, Message, Object, ticket FROM events where {0} and type = 'MASSIVE'"
        ]
        
        date_range_type = 'DATE(date_ts) = "{0}"'.format(current_date) if detailstype =="day" else 'WEEK(DATE_ts,1)=WEEK("{0}",1) and YEAR(Date_TS)=YEAR("{0}")'.format(current_date) if detailstype =="week" else 'MONTH(date_ts)=MONTH("{0}") and YEAR(Date_TS)=YEAR("{0}")'.format(current_date) if detailstype =="month" else 'date_ts >= DATE("{0}"-INTERVAL 1 YEAR)'.format(current_date)

        #import pdb;pdb.set_trace()
        ts_alert_details_rows =[]
        if type =="multiple":
            cursor.execute(ts_alert_details_query[2].format(date_range_type))
        elif type =="unique":
            cursor.execute(ts_alert_details_query[3].format(date_range_type))
        elif type =="under reboot":
            cursor.execute(ts_alert_details_query[1].format(date_range_type))
        elif type =="in maintenance":
            cursor.execute(ts_alert_details_query[0].format(date_range_type))
        elif type =="massive":
            cursor.execute(ts_alert_details_query[4].format(date_range_type))
        
        print("ts_alert_details_rows")
        ts_alert_details_rows = [list(e) for e in cursor.fetchall()]
        
        print(ts_alert_details_rows)
        for elem in ts_alert_details_rows :
            elem[0] = elem[0].split('.')[0].upper()
            elem[3] = (elem[3])[:80]

        data = {'ts_alert_details':ts_alert_details_rows}
        #print(ts_alert_details_rows)
    finally:
        conn.close()
    return data

def get_ts_ticket_details(type,detailstype):
    conn = get_autobot_connection()
    try:
        cursor = conn.cursor()
        
        ts_ticket_details_query = ["select ev.Server, date_ts, ev.message, ev.object, ev.ticket, 'SOC' from events ev where type = 'Creation' and {0} UNION select ev.Server, date_ts, ev.message, ev.object, ev.ticket, 'SOC' from events ev where type = 'Multiple' and {0} group by ticket UNION select ev.Server, date_ts, ev.message, ev.object, ev.ticket, 'SOC' from events ev where type = 'Massive' and {0} group by ticket;"
        ,"select ev.Server, date_ts, ev.message, ev.object, ev.ticket, hst.status from history hst, events ev where type = 'Closed' and hst.action = 'Forward ticket to group' and hst.id = ev.id and {0} group by ticket;"
        ,"select ev.Server, date_ts, ev.message, ev.object, ev.ticket, hst.status from events ev, history hst where type = 'First analyse' and ev.id = hst.id and hst.action = 'Forward ticket to group' and {0} group by ticket;"
        ,"select ev.Server, date_ts, ev.message, ev.object, ev.ticket, 'No Forward' from events ev where type = 'Increment' and {0};"
        ,"select ev.Server, date_ts, ev.message, ev.object, ev.ticket, 'No Forward' from history hst, events ev where action = 'Reopening Ticket' and ev.id = hst.id and {0};"
        ,"select ev.Server, date_ts, ev.message, ev.object, ev.ticket, hst.status from events ev, history hst where type = 'Forward' and ev.id = hst.id and hst.action = 'Forward ticket to group' and {0} group by ticket;"
        ]
        
        date_range_type = 'DATE(date_ts) = "{0}"'.format(current_date) if detailstype =="day" else 'WEEK(DATE_ts,1)=WEEK("{0}",1) and YEAR(DATE_ts)=YEAR("{0}")'.format(current_date) if detailstype =="week" else 'MONTH(date_ts)=MONTH("{0}") and YEAR(DATE_ts)=YEAR("{0}")'.format(current_date) if detailstype =="month" else 'date_ts >= DATE("{0}"-INTERVAL 1 YEAR)'.format(current_date)   

        #import pdb;pdb.set_trace()
        if type =="Only creation":
            cursor.execute(ts_ticket_details_query[0].format(date_range_type))
        elif type =="Remediation":
            cursor.execute(ts_ticket_details_query[1].format(date_range_type))
        elif type =="First Analyse":
            cursor.execute(ts_ticket_details_query[2].format(date_range_type))
        elif type =="Increment":
            cursor.execute(ts_ticket_details_query[3].format(date_range_type))
        elif type =="Reopen":
            cursor.execute(ts_ticket_details_query[4].format(date_range_type))
        elif type =="Forward":
            cursor.execute(ts_ticket_details_query[5].format(date_range_type))

        ts_ticket_details_rows = [list(e) for e in cursor.fetchall()]
        
        print(ts_ticket_details_rows)
        for elem in ts_ticket_details_rows :
            elem[0] = elem[0].split('.')[0].upper()
            elem[3] = (elem[3])[:80]
                        
            if elem[5] == "SOC":
                elem[5] = "No Forward"
            elif 'CLVCP' in elem[5]:
                elem[5] = "No Forward"
            elif 'No Forward' in elem[5]:
                elem[5] = "No Forward"
            else:    
                elem[5] = (elem[5])[9:]

        data = {'ts_ticket_details':ts_ticket_details_rows}
        print(ts_ticket_details_rows)
    finally:
        conn.close()
    return data

def get_problem_details(type,detailstype):

    conn = get_autobot_connection()
    conn_deis = get_deis_connection()
    
    try:
        cursor = conn.cursor()
        cursor_deis = conn_deis.cursor()
        
        details_query = ["Select hst.Problem, hst.Server, hst.date, count(hst2.status) as cnt from history_problem hst, history_problem hst2 where hst.action = 'Problem Creation' and hst.problem = hst2.problem and hst2.action = 'Ticket linked to problem' and {0} group by problem order by cnt desc",
                        "Select pb.sequence, srv.machine, pb.date_closed, count(inc.sequence) as cnt from _SMDBA_._PROBMGMT_ pb, _SMDBA_._CICHGPMLNK_ link, _SMDBA_._INVENTOR_ srv, _SMDBA_._TELMASTE_ inc where pb.state = 'C' and link.SEQ_INVITEM = srv.sequence and pb.sequence = link.SEQ_PM and inc.[SEQ_PM:] = pb.sequence and inc.SEQ_CONFIGCUST = srv.sequence and pb.[SEQ_SUPPORTSTAF] = (select sequence from _SMDBA_._PERSONNEL_ where CODE = 'AUTOBOT')and {0} group by pb.sequence, srv.machine, pb.date_closed",
        ]
        
        date_range_type = 'YEARWEEK(hst.DATE,1)=YEARWEEK("{0}")'.format(current_date) if detailstype =="week" else 'MONTH(hst.date)=MONTH("{0}")'.format(current_date) if detailstype =="month" else 'YEAR(hst.date) = YEAR("{0}")'.format(current_date)   
        date_range_type_DEIS = 'datepart (wk, DATE_CLOSED) = datepart ( wk, getdate() )' if detailstype =="week" else 'datepart (mm, DATE_CLOSED) = datepart ( mm, getdate() )' if detailstype =="month" else 'datepart (yy, DATE_CLOSED) = datepart ( yy, getdate() )'   
        
        if type =="Open":
            cursor.execute(details_query[0].format(date_range_type))
            details_rows = [list(e) for e in cursor.fetchall()]
        elif type =="Closed":
            cursor_deis.execute(details_query[1].format(date_range_type_DEIS))
            details_rows = [list(e) for e in cursor_deis.fetchall()]

        print(details_rows)
        
        data = {'problem_details':details_rows}
    finally:
        conn.close()
        conn_deis.close()
    return data

def get_top_problem_details(type,detailstype):

    conn = get_autobot_connection()
    
    try:
        cursor = conn.cursor()
        
        details_query = "Select hst1.problem, ev.Date_TS, ev.server, ev.Object, ev.message, hst2.status from history_problem hst1, history_problem hst2, events ev where ev.server like '%{0}%' and hst1.Problem = hst2.Problem and hst1.action = 'Problem Creation' and hst2.action = 'Ticket linked to problem' and ev.ticket = hst2.status and {1} group by ev.ts_alert order by hst1.Problem"
        
        date_range_type = 'YEARWEEK(ev.Date_TS,1)=YEARWEEK("{0}")'.format(current_date) if detailstype =="week" else 'MONTH(ev.Date_TS)=MONTH("{0}")'.format(current_date) if detailstype =="month" else 'YEAR(ev.Date_TS) = YEAR("{0}")'.format(current_date)   

        cursor.execute(details_query.format(type,date_range_type))
        details_rows = [list(e) for e in cursor.fetchall()]

        print(details_rows)
        
        data = {'top_problem_details':details_rows}
    finally:
        conn.close()
    return data
 
def load_data_day():
    ticket_trend_query = []
    
    ticket_trend_query_month = ["select Month(`DATE`), count(*) from history where action = 'Forward ticket to group' and status = 'SOC' and (`DATE` >= '2019-01-20' - INTERVAL 2 MONTH) Group by Month(`DATE`)"
    ,"select Month(`DATE`), count(*) from history where action = 'Ticket Closed' and ( `DATE` >= '2019-01-20' - INTERVAL 2 MONTH) Group by Month(`DATE`)"
    ,"select Month(`DATE`), count(*) from history where Status like 'First analysis%' and (`DATE` >= '2019-01-20' - INTERVAL 2 MONTH) Group by Month(`DATE`)"
    ,"select Month(`DATE`), count(*) from history where action = 'Ticket Deis already Exist' and (`DATE` >= '2019-01-20' - INTERVAL 2 MONTH) Group by Month(`DATE`)"
    ,"select Month(`DATE`), count(*) from history where action = 'Reopening Ticket' and (`DATE` >= '2019-01-20'- INTERVAL 2 Month) Group by Month(`DATE`)"
    ,"select Month(hst.DATE), count(*) from events ev, history hst, history hst2 where ev.id = hst.id and ev.id = hst2.id and ev.id NOT IN (Select id from history where status like 'First analysis%' or action = 'Ticket Closed' ) and hst.action = 'Forward ticket to group' and hst.status != 'SOC' and hst2.action = 'Forward ticket to group' and (hst.DATE >= '2019-01-20'- INTERVAL 3 Month) Group by Month(hst.DATE)"
    ]
   
    ticket_trend_query_week = ["select Week(`DATE`), count(*) from history where action = 'Forward ticket to group' and status = 'SOC' and (`DATE` >= '2019-01-20' - INTERVAL 2 WEEK) GROUP BY Week(`DATE`,1)"
    ,"select Week(`DATE`), count(*) from history where action = 'Ticket Closed' and (`DATE` >= '2019-01-20' - INTERVAL 2 WEEK) GROUP BY Week(`DATE`,1)"
    ,"select Week(`DATE`), count(*) from history where Status like 'First analysis%' and (`DATE` >= '2019-01-20' - INTERVAL 2 WEEK) Group by Week(`DATE`,1);"
    ,"select Week(`DATE`), count(*) from history where action = 'Ticket Deis already Exist' and (`DATE` >= '2019-01-20' - INTERVAL 2 WEEK) Group by Week(`DATE`,1);"
    ,"select Week(`DATE`), count(*) from history where action = 'Reopening Ticket' and (`DATE` >= '2019-01-20'- INTERVAL 2 Week) Group by Week(`DATE`,1);"
    ,"select Week(hst.DATE), count(*) from events ev, history hst, history hst2 where ev.id = hst.id and ev.id = hst2.id and ev.id NOT IN (Select id from history where status like 'First analysis%' or action = 'Ticket Closed' ) and hst.action = 'Forward ticket to group' and hst.status != 'SOC' and hst2.action = 'Forward ticket to group' and (hst.DATE >= '2019-01-20'- INTERVAL 2 Week) Group by Week(hst.DATE,1);"]

    conn = get_autobot_connection()
    cursor = conn.cursor()
    
    tab = [ticket_trend_query_week,ticket_trend_query_month]
    query_day = " SELECT DAYOFYEAR('2019-01-20')";
    cursor.execute(query_day)
    cday = cursor.fetchone()
    tickets = []
    for i in range(0,2):
        arr_result = []
        ticket_trend_query = tab[i]

        #only creation ticket trend: created ticket number - closed ticket number
        #remediation
        cursor.execute(ticket_trend_query[1])
        closed_ticket_row = cursor.fetchall()
        closed_ticket_trend = correctArray(closed_ticket_row,i)#[e[1] for e in closed_ticket_row]
        
        #created
        cursor.execute(ticket_trend_query[0])
        year_created_ticket_row = cursor.fetchall()
        year_created_ticket_trend = correctArray(year_created_ticket_row,i) #[e[1] for e in year_first_ticket_row]

        #closed
        cursor.execute(ticket_trend_query[1])
        year_closed_ticket_row = cursor.fetchall()
        year_closed_ticket_trend = correctArray(year_closed_ticket_row,i) #[e[1] for e in year_first_ticket_row]
        
        #first analyse ticket trend
        cursor.execute(ticket_trend_query[2])
        year_first_ticket_row = cursor.fetchall()
        year_first_ticket_trend = correctArray(year_first_ticket_row,i) #[e[1] for e in year_first_ticket_row]
        
        #increment ticket trend   
        cursor.execute(ticket_trend_query[3])
        year_increment_ticket_row = cursor.fetchall()
        year_increment_ticket_trend = correctArray(year_increment_ticket_row,i)#[e[1] for e in year_increment_ticket_row]
                
        #reopen ticket trend
        cursor.execute(ticket_trend_query[4])
        year_reopen_ticket_row = cursor.fetchall()
        year_reopen_ticket_trend = correctArray(year_reopen_ticket_row,i)#[e[1] for e in year_reopen_ticket_row]
        
        #forward ticket trend      
        cursor.execute(ticket_trend_query[5])
        year_forward_ticket_row = cursor.fetchall()
        year_forward_ticket_trend = correctArray(year_forward_ticket_row,i)#[e[1] for e in year_forward_ticket_row]
        
        
        arr_result.append(year_created_ticket_trend)
        arr_result.append(year_forward_ticket_trend)
        
        arr_result.append(year_first_ticket_trend)
        arr_result.append(year_increment_ticket_trend)
        
        arr_result.append(year_closed_ticket_trend)
        arr_result.append(year_reopen_ticket_trend)
        """
        sumYear = 0
        for i in alert_cnt["year"]:
            sumYear += i
        """
        tickets.append(arr_result)
    return tickets

def correctArray(arr,datee):
    conn = get_autobot_connection()
    cursor = conn.cursor()
    t1 = 0
    
    if datee == 0:
        t = [0]*3
        t1 = [0]*3
        t1 = (datetime(2019,1,20) + timedelta(days=-14)).isocalendar()[1], (datetime(2019,1,20) + timedelta(days=-7)).isocalendar()[1], (datetime(2019,1,20)).isocalendar()[1]
    
    elif datee == 1:
        t = [0]*3
        t1 = [0]*3
        now = datetime(2019,1,20).month
        t1[0] = now -2
        t1[1] = now - 1
        t1[2] = now

        if t1[0] == -1:
            t1[0] = 11
            t1[1] = 12
            
        if t1[0] == 0:
            t1[0] = 12
            t1[1] = 1
       
    elif datee == 2:
        t = [0]*7
        t1 = [0]*7
        t1 = (datetime(2019,1,20) - timedelta(days=6)).timetuple().tm_yday, (datetime(2019,1,20) - timedelta(days=5)).timetuple().tm_yday,(datetime(2019,1,20) - timedelta(days=4)).timetuple().tm_yday,(datetime(2019,1,20) - timedelta(days=3)).timetuple().tm_yday,(datetime(2019,1,20) - timedelta(days=2)).timetuple().tm_yday,(datetime(2019,1,20) - timedelta(days=1)).timetuple().tm_yday,(datetime(2019,1,20)).timetuple().tm_yday

    elif datee == 3:
        t = [0]*2
        t1 = [0]*2
        t1 = 0,(datetime(2019,1,20)).year

    index = 0
    
    for row in enumerate(t1):
        t[index] = 0
        for counter,row1 in enumerate(arr):
            if row[1] == row1[0]:
                t[index] = row1[1]
        index = index+1
    return t
